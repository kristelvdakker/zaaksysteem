BEGIN;
    ALTER TABLE filestore ADD storage_location TEXT[];
    UPDATE filestore SET storage_location = '{"LocalUStore"}';
    CREATE INDEX filestore_storage_idx ON filestore USING GIN(storage_location);
COMMIT;
