BEGIN;

CREATE INDEX object_acl_entry_groupname_idx ON object_acl_entry(groupname);

COMMIT;
