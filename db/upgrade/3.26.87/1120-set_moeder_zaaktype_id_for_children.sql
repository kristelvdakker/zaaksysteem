/*
    This script will find all active casetype_settings in zaaktypen_nodes and
    sort them on last modified, and only select the one with rank #1.
    
    we are only interested in:
        moeder_zaaktype_node_id,
        moeder_zaaktype_id,
        child_casetype_id,
        child_zaaktype_node_id
    which will provide enough information to update all child_zaaktype_node_id's
    and set their moeder_zaaktype_id
    
    thanks Martijn for JSON magic and the Window function ( RANK() OVER(...) )
*/

BEGIN;

/*
    Wipe any previous settings, they might be wrong!
*/

UPDATE zaaktype_node
    SET moeder_zaaktype_id = NULL;

WITH

/*
    expand rows for 'child_casetypes' JSON properties
*/

all_mother_and_children_ids AS (
    SELECT
        zaaktype_id AS mother_casetype_id,
        last_modified,
        (jsonb_array_elements(properties::jsonb->'child_casetypes')::jsonb->'casetype'->>'id')::int AS child_casetype_id
    FROM
        zaaktype_node
    WHERE
        properties::jsonb->>'child_casetypes' IS NOT NULL
        AND
        properties::jsonb->>'is_casetype_mother' = '1'
        AND
        deleted IS NULL
),

/*
    we only want the latest 'mother claim',
    ... so we rank those that have equal child_casetype_id's by 'last_modified
*/

ranked_mother_and_children_ids AS (
    SELECT
        mother_casetype_id,
        child_casetype_id,
        RANK() OVER(
            PARTITION BY child_casetype_id
            ORDER BY last_modified DESC
        ) as rank
    FROM
        all_mother_and_children_ids
),

/*
    but only select those that are lowest ranked
*/

selected_ranked_mother_and_children_ids AS (
    SELECT mother_casetype_id, child_casetype_id
    FROM ranked_mother_and_children_ids
    WHERE
        rank = 1
),

/*
    join with zaaktype, where we can find the current zaaktype_node_id
*/

mother_child_casetype_node_ids AS (
    SELECT
        mother_casetype_id,
        zaaktype_node_id AS child_casetype_node_id
    FROM selected_ranked_mother_and_children_ids
    JOIN zaaktype ON (
        child_casetype_id = zaaktype.id
    )
)

/*
    we now have 'mother_casetype_id' and 'child_casetype_node_id'
    
    update ...!
*/

UPDATE zaaktype_node
    SET
        moeder_zaaktype_id = mother_child_casetype_node_ids.mother_casetype_id
    FROM
        mother_child_casetype_node_ids
    WHERE
        mother_child_casetype_node_ids.child_casetype_node_id = zaaktype_node.id
;

COMMIT;

