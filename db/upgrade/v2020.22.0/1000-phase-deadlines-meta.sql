
BEGIN;

  ALTER TABLE zaak_meta ADD COLUMN current_deadline
    JSONB default '{}'::jsonb NOT NULL;

  ALTER TABLE zaak_meta ADD COLUMN deadline_timeline
    JSONB default '[]'::jsonb NOT NULL;

  ALTER TABLE zaak_meta ADD COLUMN last_modified
    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW();

  ALTER TABLE zaak_meta ADD CONSTRAINT zaak_meta_uniq_zaak_id UNIQUE (zaak_id);

  DELETE FROM zaak_meta where zaak_id IS NULL;

  ALTER TABLE zaak_meta ALTER COLUMN zaak_id SET NOT NULL;

  INSERT INTO zaak_meta (zaak_id)
  SELECT id FROM zaak WHERE id NOT IN
    (SELECT zaak_id FROM zaak_meta WHERE zaak_id IS NOT NULL);

  UPDATE zaak_meta
  SET current_deadline = zaak.current_deadline,
      deadline_timeline = zaak.deadline_timeline,
      last_modified = zaak.last_modified
  FROM zaak
  WHERE zaak_id = zaak.id;

  ALTER TABLE zaak DROP column current_deadline;
  ALTER TABLE zaak DROP column deadline_timeline;

COMMIT;
