BEGIN;

    CREATE INDEX IF NOT EXISTS idx_zaaktype_kenmerken_zaak_status_id ON zaaktype_kenmerken(zaak_status_id);
    CREATE INDEX IF NOT EXISTS idx_zaaktype_notificatie_zaak_status_id ON zaaktype_notificatie(zaak_status_id);
    CREATE INDEX IF NOT EXISTS idx_zaaktype_regel_zaak_status_id ON zaaktype_regel(zaak_status_id);
    CREATE INDEX IF NOT EXISTS idx_zaaktype_sjablonen_zaak_status_id ON zaaktype_sjablonen(zaak_status_id);
    CREATE INDEX IF NOT EXISTS idx_case_action_casetype_status_id ON case_action(casetype_status_id);

COMMIT;
