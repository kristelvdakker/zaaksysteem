BEGIN;

    ALTER TABLE zaaktype_kenmerken ADD object_id UUID;
    ALTER TABLE zaaktype_kenmerken ADD FOREIGN KEY(object_id) REFERENCES object_data(uuid) ON DELETE RESTRICT;
    ALTER TABLE zaaktype_kenmerken ADD object_metadata text NOT NULL DEFAULT '{}';

COMMIT;
