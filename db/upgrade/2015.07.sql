BEGIN;
    /* db/upgrade/2015.07/1001-ZS-6365-epic_verzenddatum_bij_documenteigenschappen.sql */

    ALTER TABLE file_metadata ADD COLUMN origin_date date;

    /* db/upgrade/2015.07/1001-zaak_archiefstatus.sql */

    ALTER TABLE zaak ADD archival_state TEXT;
    ALTER TABLE zaak ADD CONSTRAINT archival_state_value CHECK(archival_state IN ('overdragen', 'vernietigen'));

    UPDATE zaak SET archival_state = 'vernietigen' WHERE status = 'resolved';
    UPDATE zaak SET archival_state = 'overdragen', status = 'resolved' WHERE status = 'overdragen';

    ALTER TABLE zaak ADD _status TEXT;
    UPDATE zaak SET _status = status;
    ALTER TABLE zaak DROP COLUMN status;
    ALTER TABLE zaak RENAME _status TO status;
    ALTER TABLE zaak ALTER status SET NOT NULL;

    ALTER TABLE zaak ADD CONSTRAINT status_value CHECK(status IN ('new', 'open', 'stalled', 'resolved', 'deleted'));

    ALTER TABLE zaaktype_resultaten ADD trigger_archival BOOLEAN NOT NULL DEFAULT TRUE;

    /* db/upgrade/2015.07/1002-bibliotheek_kenmerk_options.sql */

    ALTER TABLE
        bibliotheek_kenmerken
    ADD COLUMN
        properties TEXT
    DEFAULT '{}';

    ALTER TABLE
        zaaktype_kenmerken
    ADD COLUMN
        properties TEXT
    DEFAULT '{}';

    /* db/upgrade/2015.07/1003-add_natuurlijk_persoon_id_to_adres.sql */

    -- Add natuurlijk_persoon_id to address
    ALTER TABLE adres ADD COLUMN natuurlijk_persoon_id INTEGER REFERENCES natuurlijk_persoon(id);
    ALTER TABLE gm_adres ADD COLUMN natuurlijk_persoon_id INTEGER REFERENCES gm_natuurlijk_persoon(id);

    -- Make sure every address in our db defaults to functie_adres = 'W';
    UPDATE adres SET functie_adres='W' WHERE functie_adres is null;
    UPDATE gm_adres SET functie_adres='W' WHERE functie_adres is null;

    ALTER TABLE adres ALTER COLUMN functie_adres SET NOT NULL;
    ALTER TABLE gm_adres ALTER COLUMN functie_adres SET NOT NULL;

    ALTER TABLE gm_adres ADD COLUMN deleted_on timestamp(6) without time zone;

    UPDATE adres SET natuurlijk_persoon_id=sub.id FROM (
        select adres_id,id from natuurlijk_persoon
    ) as sub where adres.id=sub.adres_id;

    /* db/upgrade/2015.07/1003-supersaas.sql */

    ALTER TABLE bibliotheek_kenmerken DROP CONSTRAINT bibliotheek_kenmerken_value_type_check;
    ALTER TABLE bibliotheek_kenmerken ADD CONSTRAINT bibliotheek_kenmerken_value_type_check CHECK (value_type IN ('text_uc', 'checkbox', 'richtext', 'date', 'file', 'bag_straat_adres', 'email', 'valutaex', 'bag_openbareruimte', 'text', 'bag_openbareruimtes', 'url', 'valuta', 'option', 'bag_adres', 'select', 'valutain6', 'valutaex6', 'valutaex21', 'image_from_url', 'bag_adressen', 'valutain', 'calendar', 'calendar_supersaas', 'bag_straat_adressen', 'googlemaps', 'numeric', 'valutain21', 'textarea', 'bankaccount', 'subject'));

    /* db/upgrade/2015.07/1004-ZS-6895-Recipient_in_zaaktype_notificatie_is_gelimiteerd.sql */
    ALTER TABLE zaaktype_notificatie ALTER COLUMN email          TYPE TEXT;
    ALTER TABLE contactmoment_email  ALTER COLUMN subject        TYPE TEXT;
    ALTER TABLE contactmoment_email  ALTER COLUMN recipient      TYPE TEXT;
    ALTER TABLE logging              ALTER COLUMN created_by     TYPE TEXT;
    ALTER TABLE logging              ALTER COLUMN modified_by    TYPE TEXT;
    ALTER TABLE logging              ALTER COLUMN deleted_by     TYPE TEXT;
    ALTER TABLE logging              ALTER COLUMN created_for    TYPE TEXT;
    ALTER TABLE logging              ALTER COLUMN onderwerp      TYPE TEXT;

    /* db/upgrade/2015.07/1005-find_recent.sql */
    /* Developer setting, not per se needed on a live system
    INSERT INTO config (parameter, value, advanced) VALUES ('recent_logging_timeout', '10', true);
    */

    /* db/upgrade/2015.07/1005-welkomsttekst_pip.sql */
    INSERT INTO config (parameter, value, advanced) VALUES ('pip_login_intro', '', true);

    /* db/upgrade/2015.07/1006-postcode_constraint.sql */
    /* Do not include this one for now, or run it on a DB per DB basis, could be
     * that the complete rollup will fail if there is a non-ascii zipcode
    ALTER TABLE adres
        ADD CONSTRAINT adres_postcode_value CHECK ( postcode ~ '^\d{4}[a-zA-Z]{2}$' );
     */

    /* db/upgrade/2015.07/1006-webodf_picker.sql */
    INSERT INTO config (parameter, value, advanced) VALUES ('use_flexpaper', '0', true);

    /* db/upgrade/2015.07/1007-ZS-4893-original_filename_to_text.sql */
    ALTER TABLE filestore ALTER COLUMN original_name TYPE TEXT;

    /* db/upgrade/2015.07/1007-transaction_error_count_index.sql */

    CREATE INDEX transaction_error_count_idx ON transaction ( error_count );

    /* db/upgrade/2015.07/1009-inactive_users.sql */

    ALTER TABLE user_entity DROP COLUMN IF EXISTS active;

    /* db/upgrade/2015.07/1010-directory_hierarchy.sql */

    ALTER TABLE directory ADD COLUMN path INTEGER[] NOT NULL DEFAULT ARRAY[]::INTEGER[];

    /* db/upgrade/2015.07/1010-zaaktype_active.sql */

    ALTER TABLE zaaktype ADD _active BOOLEAN DEFAULT true NOT NULL;
    UPDATE zaaktype SET _active = true where active = 1;
    UPDATE zaaktype SET _active = false where active = 0 or active is null;
    ALTER TABLE zaaktype DROP COLUMN active;
    ALTER TABLE zaaktype RENAME _active TO active;

    /* db/upgrade/2015.07/2000-rc2_update_result_periods.sql */
    UPDATE zaaktype_resultaten SET bewaartermijn = 93 WHERE bewaartermijn = 62;

COMMIT;
