// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import shortid from 'shortid';
import assign from 'lodash/assign';
import get from 'lodash/get';
import plannedCaseForm from './../../zsCasePlan/form';

const columns = [
  {
    id: 'label',
    label: 'Zaaktype',
  },
  {
    id: 'pattern',
    label: 'Patroon',
  },
  {
    id: 'reach',
    label: 'Bereik',
  },
];

const plannedCasesTable = (
  $q,
  openForm,
  plannedCases,
  onPlannedCaseUpdate,
  onPlannedCaseRemove,
  isDisabled,
  dateFilter,
  zsConfirm,
  contextualActionService
) => {
  const items = (plannedCases || []).map((plannedCase) => {
    let pattern;
    let reach;
    const moreThanOnce = plannedCase.interval_value > 1;
    const val = plannedCase.interval_value;

    switch (plannedCase.interval_period) {
      case 'once':
        pattern = 'Eenmalig';
        break;

      case 'days':
        pattern = `Elke ${moreThanOnce ? `${val} dagen` : 'dag'}`;
        break;

      case 'weeks':
        pattern = `Elke ${moreThanOnce ? `${val} weken` : 'week'}`;
        break;

      case 'months':
        pattern = `Elke ${moreThanOnce ? `${val} maanden` : 'maand'}`;
        break;

      case 'years':
        pattern = `Elk${moreThanOnce ? 'e' : ''} ${
          moreThanOnce ? `${val} jaren` : 'jaar'
        }`;
        break;
    }

    reach = dateFilter(plannedCase.next_run, 'dd-MM-yyyy');

    if (plannedCase.runs_left > 1) {
      reach = `Start op ${reach} (${plannedCase.runs_left} herhalingen)`;
    }

    return {
      id: shortid(),
      label: plannedCase.title,
      pattern,
      reach,
      source: plannedCase,
    };
  });

  const actions = [
    {
      name: 'edit',
      label: 'Bewerken',
      type: 'click',
      icon: 'pencil',
      click: (item, event) => {
        event.stopPropagation();
        event.preventDefault();

        openForm(
          {
            form: assign({}, plannedCaseForm, {
              fields: plannedCaseForm.fields.filter(
                (field) =>
                  ['from', 'pattern', 'has_pattern'].indexOf(field.name) !== -1
              ),
            }),
            title: 'Geplande zaak bewerken',
            defaults: {
              from: new Date(item.source.next_run),
              has_pattern: item.source.interval_period !== 'once',
              pattern: {
                every: {
                  count: Number(item.source.interval_value) || 1,
                  type: item.source.interval_period || 'days',
                },
                repeat: Number(item.source.runs_left),
              },
            },
          },
          $q
        ).then((values) => {
          return onPlannedCaseUpdate({
            $id: item.source.uuid,
            $values: assign({}, item.source.instance, {
              // incorrectly required by API, waiting on a fix
              type: 'scheduled_job',
              copy_relations: true,
              //
              next_run: values.from.toISOString(),
              interval_period: values.has_pattern
                ? get(values, 'pattern.every.type')
                : 'once',
              interval_value: values.has_pattern
                ? Number(get(values, 'pattern.every.count'))
                : 1,
              runs_left: values.has_pattern
                ? Number(get(values, 'pattern.repeat'))
                : 1,
            }),
          });
        });
      },
      visible: () => {
        return true;
      },
    },
    {
      name: 'remove',
      label: 'Verwijderen',
      type: 'click',
      icon: 'close',
      click: (item, event) => {
        event.stopPropagation();
        event.preventDefault();

        zsConfirm(
          'Weet u zeker dat u de geplande zaak wilt verwijderen?',
          'Verwijderen'
        ).then(() => {
          onPlannedCaseRemove({ $id: item.source.uuid });
        });
      },
      visible: () => {
        return true;
      },
    },
  ];

  const collectionActions = [
    {
      type: 'button',
      data: {
        label: 'Voeg toe',
        click: () => {
          contextualActionService.openAction(
            contextualActionService.findActionByName('geplande-zaak')
          );
        },
      },
      visible: !isDisabled,
    },
    {
      type: 'link',
      data: {
        href: `/api/object/search/?zapi_no_pager=1&zapi_format=csv&zql=SELECT {} FROM scheduled_job WHERE object.uuid in ("${plannedCases
          .map((p) => p.reference)
          .join('","')}")`,
        label: 'Download als .csv',
      },
      visible: plannedCases.length,
    },
  ].filter((action) => action.visible === undefined || action.visible);

  return {
    name: 'planned-cases',
    label: 'Geplande zaken',
    columns,
    items,
    actions,
    collectionActions,
  };
};

export default plannedCasesTable;
