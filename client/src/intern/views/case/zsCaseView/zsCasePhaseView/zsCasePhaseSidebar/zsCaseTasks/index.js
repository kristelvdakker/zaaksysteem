// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import reactIframeModule from './../../../../../../../shared/ui/zsReactIframe';
import template from './template.html';
import './styles.scss';

export default angular
  .module('zsCaseTasks', [angularUiRouterModule, reactIframeModule])
  .directive('zsCaseTasks', [
    '$state',
    '$stateParams',
    ($state, $stateParams) => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseUuid: '&',
          checklistResource: '&',
          selectedMilestone: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;
            let currentAction = $stateParams.action
              ? `/${$stateParams.action}`
              : '';
            let currentId = $stateParams.id ? `/${$stateParams.id}` : '';

            ctrl.onMessage = (message) => {
              if (message && message.type === 'refreshTasks') {
                this.checklistResource().reload();
              }
            };

            ctrl.getStartUrl = () => {
              return `/main/case/${this.caseUuid()}/phase/${this.selectedMilestone()}/tasks/${currentAction}${currentId}`;
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
