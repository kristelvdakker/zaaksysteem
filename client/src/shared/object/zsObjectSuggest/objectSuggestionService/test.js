// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { getBagLabel, checkData } from '.';

describe('objectSuggestionService', () => {
  describe('checkData', () => {
    test('null should return an array', () => {
      const input = null;

      expect(checkData(input)).toEqual([]);
    });

    test('empty array should return an empty array', () => {
      const input = [];

      expect(checkData(input)).toEqual([]);
    });

    test('filled array should return an unaltered array', () => {
      const input = ['foo', 'bar'];

      expect(checkData(input)).toEqual(['foo', 'bar']);
    });

    test('object with data property should return the data property', () => {
      const input = { data: ['result'] };

      expect(checkData(input)).toEqual(['result']);
    });

    test('object without data property should return an empty array', () => {
      const input = { foo: ['result'] };

      expect(checkData(input)).toEqual([]);
    });
  });

  describe('getBagLabel', () => {
    test('address with housenumber, without letter, without suffix', () => {
      const input = {
        huisnummer: '1',
        huisletter: '',
        huisnummer_toevoeging: '',
        straatnaam: 'Straat',
        postcode: '1111AA',
        plaats: 'Plaats',
      };

      expect(getBagLabel(input)).toEqual('Straat 1, 1111AA Plaats');
    });

    test('address with housenumber, with letter, without suffix', () => {
      const input = {
        huisnummer: '1',
        huisletter: 'a',
        huisnummer_toevoeging: '',
        straatnaam: 'Straat',
        postcode: '1111AA',
        plaats: 'Plaats',
      };

      expect(getBagLabel(input)).toEqual('Straat 1a, 1111AA Plaats');
    });

    test('address with housenumber, without letter, with suffix', () => {
      const input = {
        huisnummer: '1',
        huisletter: '',
        huisnummer_toevoeging: '1',
        straatnaam: 'Straat',
        postcode: '1111AA',
        plaats: 'Plaats',
      };

      expect(getBagLabel(input)).toEqual('Straat 1-1, 1111AA Plaats');
    });

    test('address with housenumber, with letter, with suffix', () => {
      const input = {
        huisnummer: '1',
        huisletter: 'a',
        huisnummer_toevoeging: '1',
        straatnaam: 'Straat',
        postcode: '1111AA',
        plaats: 'Plaats',
      };

      expect(getBagLabel(input)).toEqual('Straat 1a-1, 1111AA Plaats');
    });

    test('address without housenumber, without letter, without suffix', () => {
      const input = {
        huisnummer: '',
        huisletter: '',
        huisnummer_toevoeging: '',
        straatnaam: 'Straat',
        postcode: '1111AA',
        plaats: 'Plaats',
      };

      expect(getBagLabel(input)).toEqual('Straat, 1111AA Plaats');
    });

    test('address without postal code', () => {
      const input = {
        huisnummer: undefined,
        huisletter: undefined,
        huisnummer_toevoeging: undefined,
        straatnaam: 'Straat',
        postcode: undefined,
        plaats: 'Plaats',
      };

      expect(getBagLabel(input)).toEqual('Straat, Plaats');
    });
  });
});
