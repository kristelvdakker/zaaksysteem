// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import 'angular-mocks';
import rwdServiceModule from '.';

const camelCaseExpression = /^[a-z]+(?:[A-Z][a-z]+)*$/;
// NB: There are no height media queries,
// just use a constant value greater than 0.
const HEIGHT = 100;

function getDefaultViews(
  size,
  {
    smallScreen,
    smallScreenUp,
    smallMediumScreen,
    smallMediumScreenUp,
    mediumScreen,
    mediumScreenUp,
    largeScreen,
    largeScreenUp,
    xlScreen,
    xxlScreen,
  }
) {
  switch (size) {
    case smallScreen:
      return [
        'medium-and-down',
        'medium-and-up',
        'small-and-down',
        'small-medium-and-down',
      ];
    case smallScreenUp:
      return ['medium-and-down', 'medium-and-up', 'small-medium-and-down'];
    case smallMediumScreen:
      return ['medium-and-down', 'medium-and-up', 'small-medium-and-down'];
    case smallMediumScreenUp:
      return ['medium-and-down', 'medium-and-up', 'medium-large-and-up'];
    case mediumScreen:
      return [
        'large-and-up',
        'medium-and-down',
        'medium-and-up',
        'medium-large-and-up',
      ];
    case mediumScreenUp:
      return ['large-and-up', 'medium-and-up', 'medium-large-and-up'];
    case largeScreen:
      return [
        'large-and-up',
        'large-only',
        'medium-and-up',
        'medium-large-and-up',
      ];
    case largeScreenUp:
      return [
        'large-and-up',
        'large-only',
        'medium-and-up',
        'medium-large-and-up',
      ];
    case xlScreen:
      return [
        'large-and-up',
        'large-only',
        'medium-and-up',
        'medium-large-and-up',
        'xl-only',
      ];
    case xxlScreen:
      return [
        'large-and-up',
        'large-only',
        'medium-and-up',
        'medium-large-and-up',
        'xl-only',
        'xxl-only',
      ];
  }
}

function iterate(dictionary, callback) {
  Object.keys(dictionary).forEach((key) => {
    callback(dictionary[key], dictionary);
  });
}

describe('rwdService', () => {
  let $rootScope;
  let rwdService;
  let magicNumbers;

  function resize(size, dictionary) {
    window.resizeTo(size, HEIGHT);
    $rootScope.$apply();
    expect(rwdService.getActiveViews().sort()).toEqual(
      getDefaultViews(size, dictionary).sort()
    );
  }

  describe('by default', () => {
    beforeEach(angular.mock.module(rwdServiceModule));

    beforeEach(
      angular.mock.inject([
        '$rootScope',
        'rwdService',
        (...rest) => {
          [$rootScope, rwdService] = rest;
          magicNumbers = rwdService.getMagicNumbers();
        },
      ])
    );

    test('has a `getMagicNumbers` method that maps a sass variables object to camelCase keys and number values', () => {
      Object.keys(magicNumbers).forEach((key) => {
        expect(camelCaseExpression.test(key)).toBe(true);
        expect(typeof magicNumbers[key]).toBe('number');
      });
    });

    test('should not update before a digest', () => {
      expect(rwdService.getActiveViews().length).toBe(0);
    });

    test('uses breakpoints that differ by one', () => {
      const {
        smallScreen,
        smallScreenUp,
        mediumScreen,
        mediumScreenUp,
        largeScreen,
        largeScreenUp,
      } = magicNumbers;

      expect(smallScreenUp).toBe(smallScreen + 1);
      expect(mediumScreenUp).toBe(mediumScreen + 1);
      expect(largeScreenUp).toBe(largeScreen + 1);
    });

    test('should re-interpret the view types when the window resizes', () => {
      iterate(magicNumbers, resize);
    });
  });

  describe('can be configured', () => {
    beforeEach(
      angular.mock.module(rwdServiceModule, (rwdServiceProvider) => {
        rwdServiceProvider.parse(
          '$foo-bar: 100px; $any: "only screen and (min-width: 1px)";'
        );
      })
    );

    beforeEach(
      angular.mock.inject([
        '$rootScope',
        'rwdService',
        (...rest) => {
          [$rootScope, rwdService] = rest;
          magicNumbers = rwdService.getMagicNumbers();
        },
      ])
    );

    test('to use any custom breakpoints', () => {
      $rootScope.$apply();
      expect(rwdService.getActiveViews()).toEqual(['any']);
    });
  });
});
