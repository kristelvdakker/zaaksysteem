// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import windowUnloadModule from './../windowUnload';
import zsConfirmModule from './../../ui/zsConfirm';
import propCheck from './../propCheck';
import defaultMessage from './../windowUnload/defaultMessage';

export default angular
  .module('preventNavigation', [
    zsConfirmModule,
    windowUnloadModule,
    angularUiRouterModule,
  ])
  .factory('preventNavigation', [
    'zsConfirm',
    'windowUnload',
    '$state',
    (zsConfirm, windowUnload, $state) => {
      return (scope, fn) => {
        let unlisten;

        propCheck.throw(
          propCheck.shape({
            scope: propCheck.any,
            fn: propCheck.func,
          }),
          // don't send scope down to api-check
          // because of performance concerns
          { scope, fn }
        );

        windowUnload.register(scope, fn);

        unlisten = scope.$root.$on('$stateChangeStart', (event, ...rest) => {
          let msg = fn(event, ...rest),
            [to, toParams] = rest;

          if (msg === true) {
            msg = defaultMessage;
          }

          if (msg) {
            event.preventDefault();

            scope.$applyAsync(() => {
              zsConfirm(msg).then(() => {
                unlisten();

                $state.go(to.name, toParams);
              });
            });
          }
        });

        scope.$on('$destroy', unlisten);
      };
    },
  ]).name;
