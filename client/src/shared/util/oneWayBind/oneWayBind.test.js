// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import createBinding from '.';

describe('oneWayBind', () => {
  let func = createBinding();

  test('should return undefined when not configured', () => {
    expect(func()).toBeUndefined();
  });

  test('should return the result of the getter when configured', () => {
    let result = true;

    func(() => result);
    expect(func()).toBe(true);
    result = false;
    expect(func()).toBe(false);
  });

  test('should return an unregister function when configuring', () => {
    const result = true;
    const unregister = func(() => result);

    expect(func()).toBe(result);
    unregister();
    expect(func()).toBeUndefined();
  });

  test('should throw when anything but a function is passed', () => {
    expect(() => {
      func('foo');
    }).toThrow();
  });
});
