// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';

export default angular.module('zsTableHeader', []).directive('zsTableHeader', [
  () => {
    return {
      restrict: 'E',
      template,
      scope: {
        columns: '&',
        onColumnClick: '&',
      },
      bindToController: true,
      controller: [
        function () {
          let ctrl = this;

          ctrl.handleColumnClick = (column) => {
            ctrl.onColumnClick({ $columnId: column.id });
          };

          ctrl.getSortingLabel = (column) => {
            return column.sort
              ? `Sorteren op ${column.ariaLabel} ${
                  column.iconType === 'chevron-down' ? 'oplopend' : 'aflopend'
                }`
              : column.ariaLabel;
          };
        },
      ],
      controllerAs: 'zsTableHeader',
    };
  },
]).name;
