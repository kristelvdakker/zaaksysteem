// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import filterServiceModule from '../../shared/filterService';
import caseListItemListModule from './caseListItemList';
import flatten from 'lodash/flatten';
import template from './template.html';
import seamlessImmutable from 'seamless-immutable';
import configServiceModule from '../../shared/configService';
import appServiceModule from '../../shared/appService';
import get from 'lodash/get';
import './styles.scss';

export default angular
  .module('Zaaksysteem.mor.caseListView', [
    composedReducerModule,
    filterServiceModule,
    caseListItemListModule,
    configServiceModule,
    appServiceModule,
  ])
  .directive('caseListView', [
    'composedReducer',
    'filterService',
    'configService',
    'appService',
    (composedReducer, filterService, configService, appService) => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseGroups: '&',
          onLoadMore: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this,
              caseReducer,
              filteredCaseReducer;

            caseReducer = composedReducer({ scope }, ctrl.caseGroups).reduce(
              (groups) => {
                return groups || seamlessImmutable([]);
              }
            );

            caseReducer.subscribe((groups) => {
              if (groups) {
                filterService.addToIndex(
                  flatten(groups.map((group) => group.cases))
                );
              }
            });

            filteredCaseReducer = composedReducer(
              { scope },
              caseReducer,
              () => appService.state().filters,
              filterService.getAttributesToIndex()
            ).reduce((caseGroups, filters, attributesToIndex) => {
              let indexes = attributesToIndex
                .map((attrName) => `attributes.${attrName}`)
                .concat('case_location.nummeraanduiding.human_identifier')
                .concat('case_location.openbareruimte.human_identifier')
                .concat('number');

              return filters.length
                ? caseGroups.map((caseGroup) => {
                    let filteredCases = caseGroup.cases.filter((caseItem) => {
                      return filters.every((filter) => {
                        return indexes
                          .map((index) =>
                            String(get(caseItem.instance, index)).toLowerCase()
                          )
                          .some((val) => val.indexOf(filter) !== -1);
                      });
                    });

                    return {
                      cases: filteredCases,
                      empty: {
                        icon: 'close',
                        message:
                          'Er zijn geen zaken die voldoen aan je ingestelde filters',
                      },
                      id: caseGroup.id,
                      label: `${caseGroup.label.split(' (')[0]} (${
                        filteredCases.length
                      })`,
                      showMore: false,
                    };
                  })
                : caseGroups;
            });

            ctrl.isLoading = () => {};

            ctrl.getGroups = filteredCaseReducer.data;

            ctrl.getSupportLink = () => configService.getSupportLink();
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
