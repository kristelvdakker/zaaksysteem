const async = require('async');
const fs = require('fs');
const { join, sep } = require('path');
const swPrecache = require('sw-precache');
const {
  assign,
  includes,
  endsWith,
  first,
  flatten,
  keyBy,
  mapValues,
  uniq
} = require('lodash');

class ServiceWorkerCachePlugin {
  constructor(settings) {
    Object.assign(this, { settings });
  }

  apply(compiler) {
    const { settings } = this;
    const apps = settings.entries || [];
    const filesPerApp = mapValues(keyBy(apps), () => []);
    const root = settings.root || '.';
    const publicPath = settings.publicPath || '';

    const toAbsolutePath = fileName => join(root, publicPath, fileName);

    compiler.plugin('compilation', compilation => {
      compilation.plugin('module-asset', function() {
        const filename = arguments[1];
        const entry = this.entries[0];
        const appName = first(
          entry.context
            .replace(join(process.cwd(), 'src'), '')
            .replace(sep, '')
            .split(sep)
        );

        if (!filesPerApp[appName]) {
          return;
        }

        filesPerApp[appName].push(filename);
      });
    });

    compiler.plugin('after-emit', (compilation, callback) => {
      async.parallel(
        apps.map(app => {
          const chunk = compilation.namedChunks[app];
          const chunkId = chunk.id;
          const files = chunk.files;
          const assets = uniq(
            files
              .concat(filesPerApp[app])
              .concat(
                flatten(
                  compilation.chunks
                    .filter(
                      c =>
                        c.hasRuntime() === false &&
                        includes(
                          c.parents.map(parent => parent.id),
                          chunkId
                        )
                    )
                    .map(c => c.files)
                )
              )
              .filter(filename => !endsWith(filename, '.map'))
          );
          let globs = assets.map(toAbsolutePath);

          if (!fs.existsSync(join(root, app))) {
            fs.mkdirSync(join(root, app), error => {
              if (error) {
                console.error(error);
              }
            });
          }

          return function(cb) {
            globs = globs.concat(
              (settings.globs || []).map(glob => {
                return join(root, publicPath, glob);
              })
            );
            globs.push(...settings.external.map(toAbsolutePath));
            swPrecache.write(
              join(root, publicPath, app, 'service-worker.js'),
              assign(
                {
                  staticFileGlobs: globs,
                  stripPrefix: root.replace(sep, '/'),
                  maximumFileSizeToCacheInBytes: 8 * 1024 * 1024
                },
                settings.options
              ),
              cb
            );
          };
        }),
        callback
      );
    });
  }
}

module.exports = ServiceWorkerCachePlugin;
