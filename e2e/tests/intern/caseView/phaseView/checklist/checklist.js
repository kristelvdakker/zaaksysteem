import {
    openPageAs
} from './../../../../../functions/common/navigate';
import {
    openChecklist,
    openPhaseActions,
    openPhase
} from './../../../../../functions/intern/caseView/caseNav';
import {
    createChecklistItem,
    deleteChecklistItem,
    toggleChecklistItem
} from './../../../../../functions/intern/caseView/caseChecklist';

describe('when opening case 71', () => {

    let sidebar = $('.phase-sidebar'),
        checklist = $('.phase-sidebar .check-list');

    beforeAll(() => {

        openPageAs('admin', 71);

    });

    describe('and when inspecting the checklist in the current phase', () => {

        beforeAll(() => {

            openChecklist();

        });

        it('the casetype checklist items should not be able to be deleted', () => {

            expect(checklist.$('li:nth-child(1) .sidebar-item-action button').isDisplayed()).toBe(false);
            expect(checklist.$('li:nth-child(2) .sidebar-item-action button').isDisplayed()).toBe(false);
            expect(checklist.$('li:nth-child(3) .sidebar-item-action button').isDisplayed()).toBe(false);

        });

        it('the manual checklist items should be able to be deleted', () => {

            expect(checklist.$('li:nth-child(4) .sidebar-item-action button').isDisplayed()).toBe(true);
            expect(checklist.$('li:nth-child(5) .sidebar-item-action button').isDisplayed()).toBe(true);
            expect(checklist.$('li:nth-child(6) .sidebar-item-action button').isDisplayed()).toBe(true);

        });

        it('the checklist item counter should be equal to the number of unchecked checklist items', () => {

            expect(sidebar.$('.phase-sidebar-header-inner li:nth-child(2) .phase-sidebar-tab-counter').getText()).toEqual('4');

        });

        describe('and when deleting a checklist item', () => {

            beforeAll(() => {

                deleteChecklistItem('6');

            });

            it('the deleted checklist item should not be present', () => {

                $$('li .sidebar-item-title').getText().then( (itemTitle) => {
                    expect(itemTitle).not.toContain('Handmatig Drie');
                });

            });

            afterAll(() => {

                createChecklistItem('Handmatig Drie');

            });

        });

        describe('and when adding a checklist item', () => {

            beforeAll(() => {

                createChecklistItem('Automatisch Diakriët');

            });

            it('the added checklist item should be present', () => {

                $$('li .sidebar-item-title').getText().then( (itemTitle) => {
                    expect(itemTitle).toContain('Automatisch Diakriët');
                });

            });

            it('the added checklist item should be unchecked', () => {

                expect(checklist.$('li:nth-child(7) input[checked="checked"]').isPresent()).toBe(false);

            });

            afterAll(() => {

                deleteChecklistItem('7');

            });

        });

        describe('and when checking checklist items', () => {

            beforeAll(() => {

                toggleChecklistItem(3);

                toggleChecklistItem(4);

            });

            it('the checked checklist items should be checked', () => {

                expect(checklist.$('li:nth-child(3) input[checked="checked"]').isPresent()).toBe(true);
                expect(checklist.$('li:nth-child(4) input[checked="checked"]').isPresent()).toBe(true);

            });

            it('the checklist counter should be updated', () => {

                expect(sidebar.$('.phase-sidebar-header-inner li:nth-child(2) .phase-sidebar-tab-counter').getText()).toEqual('2');

            });

            describe('and when checking all checklist items', () => {

                beforeAll(() => {

                    toggleChecklistItem(1);

                    toggleChecklistItem(6);

                });

                it('the checklist counter should be hidden', () => {

                    expect(sidebar.$('.phase-sidebar-header-inner li:nth-child(2) .phase-sidebar-tab-counter').isDisplayed()).toBe(false);

                });

                afterAll(() => {

                    toggleChecklistItem(1);

                    toggleChecklistItem(6);

                });

            });

            afterAll(() => {

                toggleChecklistItem(3);

                toggleChecklistItem(4);

            });

        });

        describe('and when unchecking checklist items', () => {

            beforeAll(() => {

                toggleChecklistItem(2);

                toggleChecklistItem(5);

            });

            it('the unchecked checklist items should not be checked', () => {

                expect(checklist.$('li:nth-child(2) input[checked="checked"]').isPresent()).toBe(false);
                expect(checklist.$('li:nth-child(5) input[checked="checked"]').isPresent()).toBe(false);

            });

            it('the checklist counter should be updated', () => {

                expect(sidebar.$('.phase-sidebar-header-inner li:nth-child(2) .phase-sidebar-tab-counter').getText()).toEqual('6');

            });

            afterAll(() => {

                toggleChecklistItem(2);

                toggleChecklistItem(5);

            });

        });

        afterAll(() => {

            openPhaseActions();

        });

    });

    describe('and when inspecting the checklist in the resolved phase', () => {

        let checklistItemCheckboxes = $$('.phase-sidebar .check-list input[type="checkbox"]'),
            checklistItemActions = $$('.phase-sidebar .check-list .sidebar-item-action button');

        beforeAll(() => {

            openPhase(2);

            openChecklist();

        });

        it('the checklist items should be disabled', () => {

            checklistItemCheckboxes.each((checkbox) => {

                expect(checkbox.isEnabled()).toBe(false);

            });

        });

        it('the checklist items should not be deletable', () => {

            checklistItemActions.each((actionButton) => {

                expect(actionButton.isDisplayed()).toBe(false);

            });

        });

        it('there should be no input to add checklist items with', () => {

            expect($('.checklist-item-add').isDisplayed()).toBe(false);

        });

        afterAll(() => {

            openPhase(3);

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
