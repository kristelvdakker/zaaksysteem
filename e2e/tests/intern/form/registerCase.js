import {
    openPageAs
} from './../../../functions/common/navigate';
import {
    goNext
} from './../../../functions/common/form';
import {
    getSummaryValue
} from './../../../functions/intern/caseView/caseMenu';
import startForm from './../../../functions/common/startForm';

const casesContacts = [
    {
        requestor: {
            type: 'citizen',
            id: '1',
            name: 'T. Testpersoon'
        }
    },
    {
        requestor: {
            type: 'citizen',
            id: '13',
            name: 'B. Briefadres'
        }
    },
    {
        requestor: {
            type: 'citizen',
            id: '14',
            name: 'J. Kui'
        }
    },
    {
        requestor: {
            type: 'organisation',
            id: '1',
            name: 'Testbedrijf'
        }
    },
    {
        requestor: {
            type: 'organisation',
            id: '7',
            name: 'Tulsi Trekking'
        }
    },
    {
        requestor: {
            type: 'employee',
            id: '1',
            name: 'A. admin'
        }
    },
    {
        requestor: {
            type: 'employee',
            id: '1',
            name: 'A. admin'
        },
        recipient: {
            type: 'citizen',
            id: '1',
            name: 'T. Testpersoon'
        }
    },
    {
        requestor: {
            type: 'employee',
            id: '1',
            name: 'A. admin'
        },
        recipient: {
            type: 'organisation',
            id: '1',
            name: 'Testbedrijf'
        }
    }
];

casesContacts.forEach(caseContacts => {
    const { requestor, recipient } = caseContacts;

    describe(`when registering a case as a ${requestor.type}`, () => {
        beforeAll(() => {
            openPageAs();

            const data = {
                casetype: 'Basic casetype',
                requestorType: requestor.type,
                requestorId: requestor.id,
                channelOfContact: 'behandelaar'
            };

            if (recipient) {
                data.recipientType = recipient.type;
                data.recipientId = recipient.id;
            }

            startForm(data);
            goNext(2);
        });

        it('there should be a case', () => {
            expect($('.case-view').isPresent()).toBe(true);
        });

        it(`the case should have the ${requestor.type} as requestor`, () => {
            expect(getSummaryValue('Aanvrager')).toEqual(requestor.name);
        });

        if (recipient) {
            it(`the case should have the ${recipient.type} as recipient`, () => {
                expect(getSummaryValue('Ontvanger')).toEqual(recipient.name);
            });
        }
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
