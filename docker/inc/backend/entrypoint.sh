#!/bin/sh

set -e

if [ -n "$ZS_PRE_ACTION" ]; then
    $ZS_PRE_ACTION
fi

exec "$@"
