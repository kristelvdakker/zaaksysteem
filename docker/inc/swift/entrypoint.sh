#! /bin/sh

if [ ! -e /mnt/sdb1/1 ]; then
    echo "Creating swift storage directories in volume"
    mkdir /mnt/sdb1/1
    chown swift:swift /mnt/sdb1/*

    mkdir -p /srv/1/node/sdb1 /srv/1/node/sdb5
    chown -R swift:swift /srv/1
fi

echo "Executing: " "$@"
exec "$@"
