package Zaaksysteem::Test::Tools::SimpleStore;

use Zaaksysteem::Test;

use Zaaksysteem::Object::Syntax;
use Zaaksysteem::Tools::SimpleStore;
use Zaaksysteem::Types qw[UUID];

package Zaaksysteem::Test::Tools::SimpleStore::Object {
    use Moose;

    extends 'Zaaksysteem::Object';

    has $_ => (
        is => 'rw',
        isa => 'Str',
        type => 'string',
        label => $_,
        traits => [qw[OA]],
        predicate => "has_$_",
        clearer => "clear_$_"
    ) for qw[foo bar baz];

    override type => sub { return 'foo' };
};

sub test_simple_store {
    my $store = Zaaksysteem::Tools::SimpleStore->new;

    is scalar $store->_entry_count, 0,
        'entry_count() is 0 at construction';

    my $foo_id = $store->create(Zaaksysteem::Test::Tools::SimpleStore::Object->new(
        foo => 'bar'
    ));

    ok defined $foo_id && length $foo_id,
        'create() returns an id after creation of foo entry';

    ok UUID->check($foo_id), 'create() returns a UUID id after creation of foo entry';

    note sprintf('foo:id = %s', $foo_id);

    is scalar $store->_entry_count, 1,
        'entry_count() returns 1 after creation of foo entry'; 

    my $bar_id = $store->create(Zaaksysteem::Test::Tools::SimpleStore::Object->new(
        bar => 'foo'
    ));

    ok defined $bar_id && length $bar_id,
        'create() returns an id after creation of bar entry';

    note sprintf('bar:id = %s', $bar_id);

    is scalar $store->_entry_count, 2,
        'entry_count() returns 2 after creation of bar entry'; 

    ok $foo_id ne $bar_id, 'foo:id differs from bar:id';

    my $foo = $store->retrieve($foo_id);

    isa_ok $foo, 'Zaaksysteem::Test::Tools::SimpleStore::Object',
        'retrieve() returns object';

    is $foo->_zss_get('foo')->value, 'bar', 'retrieve()d entry has expected data';

    ok $store->exists(qb('foo', { cond => qb_eq('foo', 'bar') })),
        'exists() returns true for foo==bar comparison query';

    ok $store->exists(qb('foo', { cond => qb_eq('bar', 'foo') })),
        'exists() returns true for bar==foo comparison query';

    ok !$store->exists(qb('foo', { cond => qb_eq('foo', 'foo') })),
        'exists() returns false for foo==foo comparison query';

    ok !$store->exists(qb('foo', { cond => qb_eq('baz', 'bar') })),
        'exists() returns false for baz==bar comparison query';

    my @search_results = $store->search(qb('foo', { cond => qb_eq('foo', 'bar') }));

    is scalar @search_results, 1, 'search() returns 1 entry for foo==bar query';

    my ($foo_searched) = grep { $_->foo eq 'bar' } @search_results;

    ok defined $foo_searched,
        'search() returns entry with matching field value for foo==bar query';

    @search_results = $store->search(qb('foo', { cond => qb_or(
        qb_eq('foo', 'bar'),
        qb_eq('bar', 'foo')
    ) }));

    is scalar @search_results, 2,
        'search() returns 2 entries for foo==bar||bar==foo query';

    ($foo_searched) = grep { $_->foo } @search_results;
    my ($bar_searched) = grep { exists $_->{ bar } } @search_results;

    isa_ok $foo_searched, 'Zaaksysteem::Test::Tools::SimpleStore::Object',
        'search() returns matching foo entry';

    isa_ok $bar_searched, 'Zaaksysteem::Test::Tools::SimpleStore::Object',
        'search() returns matching bar entry';

    is $foo_searched->_zss_get('foo')->value, 'bar', 'search() foo entry has expected value';
    is $bar_searched->_zss_get('bar')->value, 'foo', 'search() bar entry has expected value';

    my @or_keys = $store->exists(qb('foo', { cond => qb_or(
        qb_eq('foo', 'bar'),
        qb_eq('bar', 'foo')
    ) }));

    is scalar @or_keys, 2, 'exists() returns 2 ids for foo==bar||bar==foo query';

    my $foo_id_ok = scalar grep { $_ eq $foo_id } @or_keys;
    my $bar_id_ok = scalar grep { $_ eq $bar_id } @or_keys;

    ok $foo_id_ok, 'foo:id found in exists() retval';
    ok $bar_id_ok, 'bar:id found in exists() retval';

    my @and_keys = $store->exists(qb('foo', { cond => qb_and(
        qb_eq('foo', 'bar'),
        qb_eq('bar', 'foo')
    ) }));

    is scalar @and_keys, 0, 'exists() returns 0 ids for foo==bar&&bar==foo query';

    my @updated_ids = $store->update(
        qb('foo', { cond => qb_eq('foo', 'bar') }),
        { foo => undef, baz => szg_value(string => 'xyz') }
    );

    is scalar @updated_ids, 1, 'update() returned 1 id';
    is shift @updated_ids, $foo_id, 'update() returned foo:id';

    my $entry = $store->retrieve($foo_id);

    ok defined $entry, 'retrieve() returns foo entry after update of entry';

    ok defined $entry->{ baz } && $entry->_zss_get('baz')->value eq 'xyz',
        'updated entry has new field';

    ok !defined $entry->{ foo },
        'updated entry does not have removed field';
    
    my @deleted_ids = $store->delete(qb('foo', {
        cond => qb_eq('baz', 'xyz')
    }));

    is scalar @deleted_ids, 1, 'delete() returns 1 deleted key';

    ok grep { $_ eq $foo_id } @deleted_ids,
        'delete() returns id of foo entry';

    is scalar $store->_entry_count, 1,
        'entry_count() returns 1 after deleting foo entry';

    ok !$store->exists(qb('foo', { cond => qb_eq('foo', 'bar') })),
        'exists() returns false for foo==bar comparison query';

    ok $store->exists(qb('foo', { cond => qb_eq('bar', 'foo') })),
        'exists() returns true for bar==foo comparison query after deleting foo entry';

    (@deleted_ids) = $store->delete(qb('foo', {
        cond => qb_lit('string', '1')
    }));

    is scalar @deleted_ids, 1, 'delete() returns 1 deleted key';

    ok grep { $_ eq $bar_id } @deleted_ids,
        'delete() returns id of remaining entry (bar)';

    is scalar $store->_entry_count, 0,
        'entry_count() returns 0 after deleting using always-true query';

    ok !$store->exists(qb('foo', { cond => qb_eq('bar', 'foo') })),
        'exists() returns false for bar==foo comparison query after deleting all entries';
}

sub test_store_exceptions {
    my $store = Zaaksysteem::Tools::SimpleStore->new(
        object_type => 'nup'
    );

    my $sso = Zaaksysteem::Test::Tools::SimpleStore::Object->new(
        foo => 'bar'
    );

    dies_ok {
        my $id = $store->create($sso)
    } 'store refuses to store object with mismatching type';

    dies_ok {
        my @items = $store->search(qb('sso'));
    } 'store refuses to search objects with mismatching type';

    lives_ok {
        my @items = $store->search(qb('nup'));
    } 'store accepts search for objects with matching type';

    {
        local $TODO = 'Sorting not implemented by SimpleStore';
        lives_ok {
            my @items = $store->search(qb('nup', { sort => qb_sort('foo', 'desc') }));
        } 'store accepts sort query';
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
