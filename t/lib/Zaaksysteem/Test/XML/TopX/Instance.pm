package Zaaksysteem::Test::XML::TopX::Instance;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::XML::TopX::Instance;
use Data::Random::NL qw(generate_bsn);

sub test_minimal_tmlo_export {

    my $topx = Zaaksysteem::XML::TopX::Instance->new(home => '.');

    isa_ok($topx, "Zaaksysteem::XML::TopX::Instance");

    my %data = (
        aggregatie => {
            identificatiekenmerk => 1,
            aggregatieniveau     => 'Record',
            naam                 => 'Foo',
            classificatie => [
                {
                    code         => 'my code',
                    omschrijving => 'my desription',
                    bron         => 'my source',
                },
                {
                    code         => 'my code 2',
                    omschrijving => 'my desription 2',
                    bron         => 'my source 2',
                }
            ]
        },
    );

    _test_tmlo_export($topx, \%data,
        "Minimal XML has been generated for aggregation (aggregatie)");

    %data = (
        bestand => {

            # Verplicht:
            identificatiekenmerk => 1,
            aggregatieniveau     => 'Bestand',
            naam                 => 'Foo',

            # Optioneel
            classificatie => {
                code         => 'my code',
                omschrijving => 'my desription',
                bron         => 'my source',
            },
            # Verplicht
            vorm => {
                redactieGenre => 'Onbekend',

                # Optioneel:
                verschijningsvorm => 'Plank',
                structuur         => 'Papier',
            },
            integriteit => 'corrupt',
            formaat     => {
                identificatiekenmerk => 'why',
                bestandsnaam         => {
                    naam     => 'Dubbel',
                    extensie => 'complex',
                },
                # Optioneel
                type => 'iets',
                omvang            => '5',
                bestandsformaat   => 'text/plain',
                creatieapplicatie => {
                    naam         => 'Zaaksysteem testsuite',
                    versie       => '1.2.0',
                    datumAanmaak => '2006-10-06T00:23:02Z',
                },
                fysiekeIntegriteit => {
                    algoritme   => 'md5',
                    waarde      => '1234',
                    datumEnTijd => '2006-10-06T00:23:02Z',
                },
                datumAanmaak => '2006-10-06T00:23:02Z',

                # eventPlan
                # relatie
            },
        }
    );
    _test_tmlo_export($topx, \%data,
        "Minimal XML has been generated for file (bestand)");


}

sub _test_tmlo_export {
    my ($topx, $data, $msg) = @_;

    lives_ok(
        sub {
            my $xml = $topx->tmlo_export('writer', $data);
            note $xml;
        },
        $msg,
    );

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::XML::TopX::Instance - Test ZS::Test::XML::TopX::Instance

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::XML::TopX::Instance

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
