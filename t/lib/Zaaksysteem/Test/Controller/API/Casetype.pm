package Zaaksysteem::Test::Controller::API::Casetype;

=head1 NAME

Zaaksysteem::Test::Controller::API::Casetype - Test methods

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Controller::API::Casetype;

=cut

use Moose;

extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;

use Zaaksysteem::Controller::API::Casetype;

=head1 METHODS

=head2 test__next_abandoned_casetypes

=cut

sub test__next_abandoned_casetypes {
    cmp_deeply(
        [ 
            Zaaksysteem::Controller::API::Casetype::_next_abandoned_casetypes(
                prev_abandoned => [],
                prev_children  => [],
                next_children  => [],
            )
        ] => [
        ],
        "Nada ..."
    );

    cmp_deeply(
        [ 
            Zaaksysteem::Controller::API::Casetype::_next_abandoned_casetypes(
                prev_abandoned => [
                    {
                        casetype => { id => 1, title => 'zaaktype_1' },
                        moredata => 'foo',
                    },
                    {
                        casetype => { id => 2, title => 'zaaktype_2' },
                        moredata => 'bar',
                    },
                ],
                prev_children => [
                    {
                        casetype => { id => 3, title => 'zaaktype_3' },
                        moredata => 'baz',
                    },
                    {
                        casetype => { id => 4, title => 'zaaktype_4' },
                        moredata => 'foo',
                    },
                ],
                next_children => [
                    {
                        casetype => { id => 2, title => 'zaaktype_2' },
                        moredata => 'xxx',
                    },
                    {
                        casetype => { id => 4, title => 'zaaktype_4' },
                        moredata => 'qux',
                    },
                    {
                        casetype => { id => 5, title => 'zaaktype_5' },
                        moredata => 'bar',
                    },
                ],
            )
        ] => [
                {
                    casetype => { id => 1, title => 'zaaktype_1' },
                    moredata => 'foo',
                },
                {
                    casetype => { id => 3, title => 'zaaktype_3' },
                    moredata => 'baz',
                },
        ],
        "Un-abandon one, add one, do not add previous / next children"
    );

}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

1;
