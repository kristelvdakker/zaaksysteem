package Zaaksysteem::XML::Zaaksysteem::Serializer;
use Zaaksysteem::Moose;

use Zaaksysteem::XML::Compile;
use Encode;

with 'Zaaksysteem::Moose::Role::Schema';

=head1 ATTRIBUTES

=head2 xential

The Xential SOAP implementation

=cut

has xml => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return Zaaksysteem::XML::Compile->xml_compile->add_class(
            'Zaaksysteem::XML::Zaaksysteem::Instance')->zaaksysteem;
    }
);

sub _generate_header {
    my $self = shift;
    return (
        Header => {
            Timestamp  => DateTime->now(),
            Identifier => $self->schema->customer_config->{instance_uuid},
        },
    );
}

=head1 METHODS

=head2 case_to_xml

Serializes a case to XML.

=cut

sub case_to_xml {
    my $self = shift;
    my $case = shift;

    if ($case->is_afgehandeld) {
        throw(
            "XML/Serializer/Case/Nonexistent",
            'You cannot serialize a closed case'
        );
    }

    my $data_for_xml = $case->TO_JSON_V0;

    my $doc = XML::LibXML::Document->new('1.1', 'UTF-8');

    my @attributes;
    foreach (keys %{$data_for_xml->{values}}) {
        my $attr = {
            magicstring => $_,
            value       => $_ eq 'case.confidentiality' ?
                $data_for_xml->{values}{$_}{mapped} :
                $data_for_xml->{values}{$_},
        };
        $attr->{value} = $doc->createCDATASection($attr->{value})
            if $attr->{value};
        push(@attributes, $attr);
    }

    return decode_utf8(
        $self->xml->case_attributes(
            'writer',
            {
                $self->_generate_header(),
                Case => {
                    UUID      => $case->get_column('uuid'),
                    Attribute => \@attributes,
                },
            }
        )
    );
}

=head2 catalogue_to_xml

Serializes the catalogue to XML.

=cut

sub catalogue_to_xml {
    my $self = shift;

    my $rs = $self->build_resultset('BibliotheekKenmerken')->search({deleted => undef});

    my @attributes;
    while (my $r = $rs->next) {
        my @values;

        my $dvals = $r->bibliotheek_kenmerken_values->search_rs({ active => 1 },
            { order_by => { '-asc' => 'sort_order' } });

        while (my $d = $dvals->next) {
            push(@values, $d->value);
        }

        push(
            @attributes,
            {
                type        => $r->value_type,
                magicstring => $r->magic_string,
                label       => $r->label,
                description => $r->description,
                help        => $r->help,
                multiple    => $r->type_multiple,
                system      => $r->system,
                default     => \@values,
            }
        );
    }

    return decode_utf8(
        $self->xml->catalogue_attributes(
            'writer',
            { $self->_generate_header(), Attribute => \@attributes, }
        )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, 2020 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
