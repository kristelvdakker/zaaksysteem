package Zaaksysteem::Object::Types::Company::Activity;
use Zaaksysteem::Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Types::Company::Activity - An Activity object for
companies

=head1 DESCRIPTION

Companies have so-called activities according to various Chamber of Commerce
instittutions. These include a code and a description.

=cut

has code => (
    is => 'ro',
    isa => 'Str',
    required => 1,
    traits => ['OA'],
    label => 'Activity code',
);

has description => (
    is => 'ro',
    isa => 'Str',
    required => 1,
    traits => ['OA'],
    label => 'Activity description',
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
