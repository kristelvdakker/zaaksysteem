package Zaaksysteem::Object::Types::Document;

use Moose;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Document - A document object type

=head1 DESCRIPTION

=cut

use BTTW::Tools;

use Zaaksysteem::Types qw[NonEmptyStr];

=head1 ATTRIBUTES

=head2 name

This is the name of the document without the extension and may not be filesystem safe

=cut

has name => (
    is       => 'rw',
    isa      => NonEmptyStr,
    label    => 'Name',
    traits   => [qw[OA]],
    required => 1
);

=head2 filename

Stores the name of the C<document> object. This will usually be the filename
provided during upload of the file, and should be expected to be
filesystem-safe.

=cut

has filename => (
    is       => 'rw',
    isa      => NonEmptyStr,
    label    => 'Name',
    traits   => [qw[OA]],
    required => 1
);

=head2 number

The unique number of the document

=cut

has number => (
    is       => 'rw',
    isa      => 'Int',
    label    => 'Serial number',
    traits   => [qw[OA]],
    required => 1
);

=head2 case

A reference to a case object

=cut

has case => (
    is => 'rw',
    type      => 'case',
    label     => 'Case',
    traits    => [qw[OR]],
    required  => 0,
    predicate => 'has_case',
);

=head2 version

The version of the document

=cut

has version => (
    is        => 'rw',
    isa       => 'Int',
    label     => 'Version number',
    traits    => [qw[OA]],
    required  => 0,
    predicate => 'has_version',
);

=head2 file

A reference to the file object on disk

=cut

has file => (
    is       => 'rw',
    type     => 'file',
    label    => 'File',
    traits   => [qw[OR]],
    required => 1
);

=head2 metadata

The meta data of the file

=cut

has metadata => (
    is        => 'rw',
    type      => 'document/metadata',
    label     => 'Metadata',
    traits    => [qw[OR]],
    required  => 0,
    predicate => 'has_metadata',
    embed     => 1,
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
