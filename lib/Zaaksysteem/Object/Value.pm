package Zaaksysteem::Object::Value;

use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Object::Value - Abstracted object value class

=head1 DESCRIPTION

The object infrastructure uses boxed/wrapped/abstracted notions of values,
this class is a data holder for such values.

=cut

use Zaaksysteem::Types qw[ValueType];

=head1 ATTRIBUTES

=head2 type

Reference to a L<Zaaksysteem::Interface::ValueType>-implementing object.

=head3 Delegates

=over 4

=item type_name

See L<Zaaksysteem::Interface::ValueType/name>.

=back

=cut

has type => (
    is => 'rw',
    isa => ValueType,
    required => 1,
    handles => {
        type_name => 'name',
    }
);

=head2 value

Holder of the Perl-level value abstracted by an instance of this class.

=head3 Delegates

=over 4

=item has_value

Predicate that returns true if any value is set (including C<undef>).

=back

=cut

has value => (
    is => 'rw',
    clearer => 'clear_value',
    predicate => 'has_value'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
