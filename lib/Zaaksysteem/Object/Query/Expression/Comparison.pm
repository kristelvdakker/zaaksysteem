package Zaaksysteem::Object::Query::Expression::Comparison;

use Moose;

use BTTW::Tools;

use Moose::Util::TypeConstraints qw[
    enum
    find_type_constraint
    role_type
];

with 'Zaaksysteem::Object::Query::Expression';

=head1 NAME

Zaaksysteem::Object::Query::Expression::Equal - Abstracts quality testing
conditonal

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 mode

Mode of comparison. One of C<equal>, C<not_equal>, C<less_than>,
C<greater_than>, C<equal_or_less_than>, or C<equal_or_greater_than>.

=cut

has mode => (
    is => 'rw',
    isa => enum([qw[
        equal
        not_equal
        less_than
        greater_than
        equal_or_less_than
        equal_or_greater_than
    ]]),
    default => 'equal',
    required => 1
);

=head2 expressions

Collection of expressions that are tested for equality.

=cut

has expressions => (
    is => 'rw',
    isa => find_type_constraint('ArrayRef')->parameterize(
        role_type('Zaaksysteem::Object::Query::Expression')
    ),
    required => 1,
    traits => [qw[Array]],
    handles => {
        all_expressions => 'elements'
    }
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Zaaksysteem::Object::Query::Expression/stringify>.

    # "my_field equal to 'abc'"
    qb_eq('my_field', 'abc');

=cut

sub stringify {
    my $self = shift;

    return join sprintf(' %s ', $self->mode), map {
        $_->stringify
    } $self->all_expressions;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
