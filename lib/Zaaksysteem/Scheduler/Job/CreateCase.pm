package Zaaksysteem::Scheduler::Job::CreateCase;
use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Scheduler::Job::CreateCase - Role for a ScheduledJob to create a new case

=head1 SYNOPSIS

    my $sj = Zaaksysteem::Object::Types::ScheduledJob->new(
        job             => 'CreateCase',
        next_run        => $now->clone,
        interval_period => 'weeks',
        interval_value  => 2,
        runs_left       => 2,
    );
    ensure_all_roles($sj, 'Zaaksysteem::Scheduler::Job::CreateCase');

    # Make sure at least a CaseType and a Case ("source" case) are related
    $sj->relate(
        $casetype_object,
        relationship_name_a => 'job_dependency',
        relationship_name_b => 'scheduled_by',
    );
    $sj->relate(
        $case_object,
        relationship_name_a => 'job_dependency',
        relationship_name_b => 'scheduled_by',
    );
    # Other objects can be linked like this:
    $sj->relate(
        $random_other_object,
        relationship_name_a => 'job_dependency',
        relationship_name_b => 'scheduled_by',
    );

    my $newly_created_case = $sj->run($dummy_context);

=cut

use BTTW::Tools;

=head1 METHODS

=head2 run

Run the scheduled job: create a new case.

The ScheduledJob this role is applied to must have at least a related
"casetype" object and a related "case" object, to know what kind of case to
create, and to link it with the old one.

=cut

sub _get_case_from_relations {
    my ($self, $c) = @_;

    my @cases = $self->filter_relations(
        sub {
            $_->related_object_type eq 'case'
                && $_->relationship_name_b eq 'scheduled_by';
        }
    );

    if (@cases != 1) {
        throw(
            'scheduler/job/create_case/originator_case',
            sprintf('CreateCase job %s not have exactly one "originator" case', $self->id//'<unsaved>'),
        );
    }

    my $uuid =  $cases[0]->related_object_id;

    my $case = $c->model('DB::Zaak')->search_rs(
        { uuid => $uuid, , deleted => undef },
        { rows => 1 }
    )->first;

    return $case if $case;
    throw("scheduler/job/create_case/originator_case/not_found",
        "Unable to find case with uuid '$uuid'");
}

sub _get_casetype_from_relations {
    my ($self, $c) = @_;
    my @casetypes = $self->filter_relations(
        sub { $_->related_object_type eq 'casetype' }
    );

    if (@casetypes != 1) {
        throw(
            'scheduler/job/create_case/relations',
            sprintf(
                'CreateCase job %s not have exactly one related "casetype"',
                $self->id // '<unsaved>'),
        );
    }

    my $uuid = $casetypes[0]->related_object_id;
    my $casetype = $c->model('DB::Zaaktype')->search_rs(
        { uuid => $uuid, deleted => undef, active => 1},
        { rows => 1 }
    )->first;
    return $casetype if $casetype;
    throw("scheduler/job/create_case/relations/casetype/not_found",
        "Unable to find casetype with uuid '$uuid'");
}

sub run {
    my ($self, $c) = @_;

    my $casetype  = $self->_get_casetype_from_relations($c);
    my $case      = $self->_get_case_from_relations($c);
    my $requestor = $case->aanvrager_object->betrokkene_identifier;

    my $new_case = $c->model('DB::Zaak')->create_zaak(
        {
            aanvraag_trigger => 'extern',
            aanvragers       => [
                {
                    betrokkene  => $requestor,
                    verificatie => 'medewerker',
                }
            ],
            registratiedatum         => DateTime->now(),
            contactkanaal            => 'behandelaar',
            zaaktype_id              => $casetype->id,

            relatie                  => 'vervolgzaak_datum',
            actie_kopieren_kenmerken => 0,
            zaak_id                  => $case->id,
        }
    );

    my @other_relations = $self->filter_relations(
        sub {
               ($_->related_object_type ne 'casetype')
            && ($_->related_object_type ne 'case')
        }
    );

    return $new_case unless @other_relations;

    my $rs = $c->model('DB')->resultset('ObjectRelationships');
    my $uuid = $new_case->get_column('uuid');

    for my $relation (@other_relations) {

        $rs->create(
            {
                owner_object_uuid => $uuid,

                object1_uuid      => $uuid,
                object1_type      => 'case',
                type1             => 'embeds',

                object2_uuid      => $relation->related_object_id,
                object2_type      => $relation->related_object_type,
                type2             => 'embedded',
            }
        );
    }

    return $new_case;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
