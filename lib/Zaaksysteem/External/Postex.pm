package Zaaksysteem::External::Postex;
use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::External::Postex - A Postex integration module

=head1 DESCRIPTION

Integrate L<Postex> into Zaaksysteem. This module is the glue
layer between any module that wants to use Postex into
Zaaksysteem

=head1 SYNOPSIS

    use Zaaksysteem::External::Postex;

    my $model = Zaaksysteem::External::Postex->new(
        secret   => 'my-api-key',
        endpoint => 'https://some.postex.nl/api',
    );


=cut

use BTTW::Tools::UA qw(new_user_agent);
use HTTP::Request::Common;
use JSON::MaybeXS qw(encode_json decode_json);
use MIME::Base64 qw(encode_base64);
use Zaaksysteem::Types qw(URIx);

=head1 ATTRIBUTES

=head2 secret

Your API key from Postex. Required.

=cut

has secret => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 endpoint

A Postex API endpoint. Required.

=cut

has endpoint => (
    is       => 'ro',
    isa      => 'URI',
    required => 1,
);

=head2 ua

An LWP::UserAgent, build for you.

=cut

has 'ua' => (
    is      => 'ro',
    isa     => 'LWP::UserAgent',
    lazy    => 1,
    builder => '_build_ua',
);

sub _build_ua {
    my $self = shift;
    my $ua = new_user_agent(
        agent   => sprintf('Zaaksysteem/%s', $Zaaksysteem::VERSION),
        timeout => 30,
    );
    $ua->default_header('Content-Type' => 'application/json');
    $ua->default_header(Accept         => 'application/json');
    $ua->default_header( Authorization  => "Bearer " . $self->secret);
    return $ua;
}


=head2 send_case_document

Implements all the logic to create a Postex document request. Returns a
payload hashref containing the messagestructure that can be delivered to Postex

=cut

define_profile send_case_document => (
    required => {
        case             => 'Zaaksysteem::Zaken::ComponentZaak',
        subject          => 'Str',
        body             => 'Str',
        file_attachments => 'Int',
    },
);

sub send_case_document {
    my $self = shift;
    my $options = assert_profile({@_})->valid;

    my $case = $options->{case};

    my $payload = {
        case      => $self->_build_case_metadata($case),
        recipient => $self->_build_recipient_data($case->aanvrager_object),
        message   => {
            'subject'   => $options->{subject},
            'body'      => $options->{body}
        },
        'files'     => $self->_build_files_metadata($case, $options->{file_attachments})
    };

    my $result = $self->_send_message($payload);

    $case->trigger_logging('case/send_postex', {
        component => 'zaak',
        data => {
            zaak_id    => $case->id,
            ptx_result => $result,
            subject    => $options->{subject},
            body       => $options->{body},
            filenames  => [ map { $_->{name } } @{ $payload->{files} } ],
        }
    });

    return {
        'request' => $payload,
        'response' => $result
    };
}

sub _send_request {
    my ($self, $req) = @_;

    my $res = $self->ua->request($req);
    return decode_json($res->decoded_content) if $res->is_success;

    $self->log->info($res->as_string);
    throw('postex/response/invalid', $res->status_line);
}

sub _send_message {
    my ($self, $payload) = @_;

    my $json = encode_json($payload);

    return $self->_send_request(
        POST(
            $self->endpoint,
            Content        => $json,
            'Content-Type' => 'application/json',
        )
    );
}

=head2 _build_case_metadata

Build hash with information about a case

=cut

sub _build_case_metadata {
    my ($self, $case) = @_;
    return {
        id              => $case->id,
        case_type_id    => $case->zaaktype_node_id->id,
        case_type_title => $case->zaaktype_node_id->titel
    };
}

=head2 _build_recipient_data_np

Build recipient metadata for NatuurlijkPersoon

=cut

sub _build_recipient_data_np {
    my ($self, $aanvrager) = @_;

    my $adres = $aanvrager->is_briefadres
        ? $aanvrager->correspondentieadres
        : $aanvrager->verblijfsadres;

    return {
        type            => 'natuurlijk_persoon',
        bsn             => $aanvrager->bsn,
        display_name    => $aanvrager->display_name,
        voorletters     => $aanvrager->voorletters,
        naam            => $aanvrager->achternaam,
        adellijke_titel => $aanvrager->adellijke_titel,
        adres           => $self->_build_address(
            $aanvrager->is_briefadres ? 'correspondentie' : 'verblijf', $adres
        ),
    };
}

sub _build_address {
    my ($self, $type, $address) = @_;
    return {
        type                 => $type,
        straatnaam           => $address->straatnaam,
        postcode             => $address->postcode,
        woonplaats           => $address->woonplaats,
        huisnummertoevoeging => $address->huisnummertoevoeging,
        huisletter           => $address->huisletter,
        huisnummer           => $address->huisnummer,
        adres_buitenland1    => $address->adres_buitenland1,
        adres_buitenland2    => $address->adres_buitenland2,
        adres_buitenland3    => $address->adres_buitenland3,
        land                 => $self->_landnaam($address->landcode)
    };
}

=head2 _landnaam

Convert country code integer to Dutch label

=cut

sub _landnaam {
    my ($self, $landcode) = @_;
    return Zaaksysteem::Object::Types::CountryCode->new_from_dutch_code(
        $landcode
    )->label;
}


=head2 _build_recipient_data_bedrijf

Build recipient metadata for Bedrijf

=cut

sub _build_recipient_data_bedrijf {
    my ($self, $aanvrager) = @_;

    my $type    = $aanvrager->is_briefadres ? "correspondenctie" : "vestiging";
    my $address = $self->_build_address($type, $aanvrager);

    return {
        type        => 'bedrijf',
        handelsnaam => $aanvrager->handelsnaam,
        email       => $aanvrager->email,
        adres       => {
            'tav'  => $aanvrager->contact_naam,
            %$address,
        }
    };
}


=head2 _build_files_metadata

Takes a case and list of file ids. Encodes the files into base64 (in memory)
and returns an array containing a hash per file entry

Assumes there is enough memory to read a file into memory
Dies if no files are selected

=cut

sub _build_files_metadata {
    my ($self, $case, $file_ids) = @_;
    my $rs = $case->search_active_files($file_ids);

    my @files;
    while (my $file = $rs->next) {
        my $content = $file->filestore->content();
        my $base_64_content = encode_base64($content, '');

        push(
            @files,
            {
                'data'      => $base_64_content,
                'name'      => $file->name,
                'extension' => $file->extension,
                'version'   => $file->version
            }
        );
    }

    throw("postex/files/missing", "No files found in case " . $case->id)
        unless @files;

    return \@files;
}


=head2 _build_recipient_data

Builds recipient and address information block meatdata.
Delegates for NatuurlijkPersoon and Bedrijf. Otherwise die with unsupported message.

=cut

sub _build_recipient_data {
    my ($self, $aanvrager) = @_;

    if ($aanvrager->isa('Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon')) {
        return $self->_build_recipient_data_np($aanvrager->current);
    }

    if ($aanvrager->isa('Zaaksysteem::Betrokkene::Object::Bedrijf')) {
        return $self->_build_recipient_data_bedrijf($aanvrager->current);
    }

    throw('postex/subject/unsupported_type',
        "Unsupported betrokkene_type: " . $aanvrager->btype);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
