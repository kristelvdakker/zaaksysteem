package Zaaksysteem::Backend::Sysin::Modules::KoppelAppObjections;

use Moose;

use BTTW::Tools;

use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw[
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
];

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::KoppelAppObjections;

=cut

use constant INTERFACE_ID => 'koppelapp_objections';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint_get',
        type => 'text',
        label => 'Form GET URL',
        description => 'Voer hier de URL in van de KoppelApp-koppeling om gegevens over een bezwaar op te halen',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint_post',
        type => 'text',
        label => 'Form POST URL',
        description => 'Voer hier de URL in van de KoppelApp-koppeling om gegevens van een bezwaar naar terug te sturen',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint_cyclomedia',
        type => 'text',
        label => 'URL van Cyclomedia-koppeling',
        description => 'Voer hier de URL in van de koppeling om afbeeldingen op te halen',
    ),
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Koppel.app Bezwaren',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    manual_type                   => ['text'],
    module_type                   => ['apiv1', 'api'],
    is_multiple                   => 1,
    is_manual                     => 1,
    retry_on_error                => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    test_interface  => 1,

    test_definition => {
        description => 'Hier kunt u de verbinding met de externe interface testen.',

        tests => [
            {
                id => 1,
                label => 'Test verbinding met GET URL',
                name => 'test_connection_get',
                method => 'test_connection_get',
                description => 'Zaaksysteem zal proberen verbinding naar de externe partij op te zetten.'
            },
            {
                id => 2,
                label => 'Test verbinding met POST URL',
                name => 'test_connection_post',
                method => 'test_connection_post',
                description => 'Zaaksysteem zal proberen verbinding naar de externe partij op te zetten.'
            },
        ],
    },

    trigger_definition  => {
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{MODULE_SETTINGS()});
};

=head2 test_connection_get

This test case checks the configured GET URL.

=cut

sub test_connection_get {
    my $self = shift;
    my $interface = shift;

    my $url = $interface->jpath('$.endpoint_get');

    unless ($url) {
        throw('api/v1/trigger/test_connection', 'Geen "GET" URL geconfigureerd.');
    }

    return $self->test_host_port($url);
}

=head2 test_connection_post

This test case checks the configured POST URL.

=cut

sub test_connection_post {
    my $self = shift;
    my $interface = shift;

    my $url = $interface->jpath('$.endpoint_post');

    unless ($url) {
        throw('api/v1/trigger/test_connection', 'Geen "POST" URL geconfigureerd.');
    }

    return $self->test_host_port($url);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2020 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
