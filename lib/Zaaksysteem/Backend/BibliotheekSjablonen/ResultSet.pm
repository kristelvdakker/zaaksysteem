package Zaaksysteem::Backend::BibliotheekSjablonen::ResultSet;

use strict;
use warnings;

use Moose;
use Params::Profile;
use Zaaksysteem::Constants;

extends 'DBIx::Class::ResultSet';

use Exception::Class (
    'Zaaksysteem::Backend::BibliotheekSjablonen::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::BibliotheekSjablonen::ResultSet::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::BibliotheekSjablonen::ResultSet::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
);

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

