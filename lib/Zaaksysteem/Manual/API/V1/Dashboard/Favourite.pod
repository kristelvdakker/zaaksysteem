=head1 NAME

Zaaksysteem::Manual::API::V1::Favourite - Retrieval of favourites.

=head1 Description

This API-document describes the usage of our JSON Dashboard=>Favourites API. Via the Favourites API it is
possible to retrieve, create and edit favourites for a logged in user. Favourites could be a list of
casetypes, a list of contacts, etc.

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/dashboard/favourite

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head2 list

   /api/v1/dashboard/favourite/TYPE

Retrieving multiple objects from our database is as simple as calling the URL C</api/v1/dashboard/favourite> without arguments. The
C<result> property will contain an object of type B<set>, which allows us to page the data.

B<Example call>

 https://localhost/api/v1/dashboard/favourite/casetype

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-90fa13-b05b31",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 3,
            "total_rows" : 3
         },
         "rows" : [
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
                  "label" : "VKiy9CSjy7",
                  "order" : "30",
                  "reference_id" : "3194834e-e15a-48ee-8ee0-f6246c7b68ed",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
                  "label" : "ISwb7ICdj7",
                  "order" : "40",
                  "reference_id" : "820052f5-c366-435d-9537-294906f894c4",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
                  "label" : "LPdx5HXuv4",
                  "order" : "50",
                  "reference_id" : "6aeab45a-ee8a-4962-afc1-f8f52a418711",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
               "type" : "favourite"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head1 Mutate data

Mutations via our API will have to be send via the HTTP C<POST> method. The inputdata for these mutations
are one single JSON object containing the parameters. When no input is needed, make sure you send an empty
object (C< {} >)

Every mutation ends with a complete return of all favourites of the C<reference_type> "TYPE" in a C<set> type.

=head2 create

   /api/v1/dashboard/favourite/TYPE/create

It is possible to create a favourite in zaaksysteem.nl below the given type. E.g. a new favourite to
the type "casetype".

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item reference_id [required]

B<TYPE>: UUID

The UUID referencing the favourite. E.g: The casetype UUID.

=back

B<Example call>

 https://localhost/api/v1/dashboard/favourite/casetype/create

B<Request JSON JSON>

=begin javascript

{
   "reference_id" : "2806b1cf-faa0-4304-bdb2-8b24768dcdc6"
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-1d65b6-0a77e2",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 1,
            "total_rows" : 1
         },
         "rows" : [
            {
               "instance" : {
                  "id" : "2e5ab1a6-fda7-47fb-907c-53c49f6af1b3",
                  "label" : "YFcd1AKyk6",
                  "order" : 20,
                  "reference_id" : "8ecbb15f-1f78-4eb4-b369-c333cba6779d",
                  "reference_type" : "casetype"
               },
               "reference" : "2e5ab1a6-fda7-47fb-907c-53c49f6af1b3",
               "type" : "favourite"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head2 update

   /api/v1/dashboard/favourite/casetype/UUID/update

It is possible to update a single favourite in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item reference_id [required]

B<TYPE>: UUID

The new UUID of this favourite

=item order

B<TYPE>: Integer

The order of this favourite. When you would like to reorder the favourite, see the below examples.

B<Place it at the end>

   {
      order: null         // or order: 0
   }

B<Place it at the beginning>

   {
      order: 1
   }

B<Place it after one with order: 20>

   {
      order: 21
   }

=back

B<Example call>

 https://localhost/api/v1/dashboard/favourite/casetype/c25d2daf-0e00-4fa5-8bc7-b61fff072234/update

B<Request JSON JSON>

=begin javascript

{
   "reference_id" : "2806b1cf-faa0-4304-bdb2-8b24768dcdc6",
   "order": 21
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-90fa13-b05b31",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 3,
            "total_rows" : 3
         },
         "rows" : [
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
                  "label" : "VKiy9CSjy7",
                  "order" : "30",
                  "reference_id" : "3194834e-e15a-48ee-8ee0-f6246c7b68ed",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
                  "label" : "ISwb7ICdj7",
                  "order" : "40",
                  "reference_id" : "820052f5-c366-435d-9537-294906f894c4",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
                  "label" : "LPdx5HXuv4",
                  "order" : "50",
                  "reference_id" : "6aeab45a-ee8a-4962-afc1-f8f52a418711",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
               "type" : "favourite"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head2 bulk_update

   /api/v1/dashboard/favourite/TYPE/bulk_update

It is possible to update multiple favourites in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing a property C<updates>, containing
an array of hashes containing the following properties.

B<Properties>

=over 4

=item reference_id [required]

B<TYPE>: UUID

The new UUID of this favourite

=item order

B<TYPE>: Integer

The order of this favourite. When you would like to reorder the favourite, see the below examples.

B<Place it at the end>

   {
      order: null         // or order: 0
   }

B<Place it at the beginning>

   {
      order: 1
   }

B<Place it after one with order: 20>

   {
      order: 21
   }

=back

B<Example call>

 https://localhost/api/v1/dashboard/favourite/casetype/bulk_update

B<Request JSON JSON>

=begin javascript

{
   "updates" : [
      {
         "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd10",
         "reference_id" : "7806b1cf-faa0-4304-bdb2-8b24768dcd24"
      },
      {
         "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd20",
         "reference_id" : "7806b1cf-faa0-4304-bdb2-8b24768dcd25"
      }
   ]
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-90fa13-b05b31",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 3,
            "total_rows" : 3
         },
         "rows" : [
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
                  "label" : "VKiy9CSjy7",
                  "order" : "30",
                  "reference_id" : "3194834e-e15a-48ee-8ee0-f6246c7b68ed",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd30",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
                  "label" : "ISwb7ICdj7",
                  "order" : "40",
                  "reference_id" : "820052f5-c366-435d-9537-294906f894c4",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd40",
               "type" : "favourite"
            },
            {
               "instance" : {
                  "id" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
                  "label" : "LPdx5HXuv4",
                  "order" : "50",
                  "reference_id" : "6aeab45a-ee8a-4962-afc1-f8f52a418711",
                  "reference_type" : "casetype"
               },
               "reference" : "2806b1cf-faa0-4304-bdb2-8b24768dcd50",
               "type" : "favourite"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head2 delete

   /api/v1/dashboard/favourite/TYPE/UUID/delete

It is possible to delete a favourite in zaaksysteem.nl.

B<Properties>

None required

B<Example call>

 https://localhost/api/v1/dashboard/favourite/casetype/c25d2daf-0e00-4fa5-8bc7-b61fff072234/delete

B<Request JSON JSON>

=begin javascript

{}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-ea80c8-d8d303",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 0,
            "total_rows" : 0
         },
         "rows" : []
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head1 Objects

=head2 Favourite

Most of the calls in this document return an instance of type C<favourite>. Below we provide more information about
the contents of this object.

B<Properties>

=over 4

=item reference_id [required]

B<TYPE>: UUID

The new UUID of this favourite

=item order

B<TYPE>: Integer

The order of this favourite. When you would like to reorder the favourite, see the below examples.

=item label

B<TYPE>: Str

A human readable description of this favourite. E.g.: the title of a C<casetype>

=item reference_id

B<TYPE>: UUID

The id referencing to the favourite

=item reference_type

B<TYPE>: Str

The kind of favourite, e.g. C<casetype>

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Dashboard>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
