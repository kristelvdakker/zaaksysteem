package Zaaksysteem::Zaaktypen::Dependency;
use Moose;

use Params::Profile;

use namespace::autoclean;
use Clone qw(clone);
use Carp qw(cluck);
use BTTW::Tools;

with 'MooseX::Log::Log4perl';

has solution => (
    is        => 'rw',
    predicate => 'has_solution',
    clearer   => 'remove_solution',
);

has [qw/keyname type name id bibliotheek_categorie_id local_id/] => (
    'is'    => 'rw',
);

has ancestry_hash => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {} },
);


#
# ancestry is the tree location of an item. we need it to manipulate the
# zaaktype structure to inform it of the new id for the dependency.
#
# say we received id = 302 for kenmerk 'aantal kinderen'. in the local
# zaaksysteem this kenmerk is already present, but with id = 511. so every
# instance where kenmerk 502 was used now a reference has to be made to kenmerk
# 511. ancestry_hash contains the locations where these references were made
#
# A whole lot of words without an example, please let me look into your brain
# and figure out WHAT THE FUCK YOU ARE TALKING ABOUT
#
sub add_ancestry {
    my ($self, $ancestry, @keys) = @_;

    my $clone_ancestry = clone($ancestry);

    push @$clone_ancestry, @keys;
    my $joined = join ",", @$clone_ancestry;

    $self->ancestry_hash->{$joined} ||= $clone_ancestry;

}

use JSON;

#
# return references to the tree items that need modification
#
sub sub_items {
    my ($self, $zaaktype) = @_;

    my $ancestry_hash = clone $self->ancestry_hash;

    my $sub_items = [];

    foreach my $ancestry (values %$ancestry_hash) {
        die "ancestry list empty" unless @$ancestry;
        # track back to the proper tree leaf

        my $sub_item = $zaaktype;
        my $key_name = pop @$ancestry;

        my $prev_ancestor = '';
        foreach my $ancestor (@$ancestry) {

            if ($ancestor eq 'selectedUnits') {
                $sub_item = JSON::decode_json($sub_item);
            }

            if ($prev_ancestor eq 'selectedUnits') {
                $sub_item = $sub_item->[$ancestor];
            }
            else {
                $sub_item = $sub_item->{$ancestor};
            }

            $prev_ancestor = $ancestor;

        }

        push @$sub_items, {
            sub_item => $sub_item,
            key_name => $key_name
        };
    }

    return $sub_items;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 add_ancestry

TODO: Fix the POD

=cut

=head2 remove_solution

TODO: Fix the POD

=cut

=head2 sub_items

TODO: Fix the POD

=cut

