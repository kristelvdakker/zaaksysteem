use utf8;
package Zaaksysteem::Schema::Parkeergebied;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Parkeergebied

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<parkeergebied>

=cut

__PACKAGE__->table("parkeergebied");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'parkeergebied_id_seq'

=head2 bag_hoofdadres

  data_type: 'bigint'
  is_nullable: 1

=head2 postcode

  data_type: 'varchar'
  is_nullable: 0
  size: 6

=head2 straatnaam

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 huisnummer

  data_type: 'integer'
  is_nullable: 1

=head2 huisletter

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 huisnummertoevoeging

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 parkeergebied_id

  data_type: 'integer'
  is_nullable: 1

=head2 parkeergebied

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 woonplaats

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "parkeergebied_id_seq",
  },
  "bag_hoofdadres",
  { data_type => "bigint", is_nullable => 1 },
  "postcode",
  { data_type => "varchar", is_nullable => 0, size => 6 },
  "straatnaam",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "huisnummer",
  { data_type => "integer", is_nullable => 1 },
  "huisletter",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "huisnummertoevoeging",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "parkeergebied_id",
  { data_type => "integer", is_nullable => 1 },
  "parkeergebied",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "woonplaats",
  { data_type => "varchar", is_nullable => 1, size => 255 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-28 09:26:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Vgd6YdFyaHsLAMVG7eb5Qg

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::Parkeergebied');




# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

