use utf8;
package Zaaksysteem::Schema::ContactRelationship;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ContactRelationship

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<contact_relationship>

=cut

__PACKAGE__->table("contact_relationship");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'contact_relationship_id_seq'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 contact

  data_type: 'integer'
  is_nullable: 0

=head2 contact_uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 contact_type

  data_type: 'text'
  is_nullable: 0

=head2 relation

  data_type: 'integer'
  is_nullable: 0

=head2 relation_uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 relation_type

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "contact_relationship_id_seq",
  },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "contact",
  { data_type => "integer", is_nullable => 0 },
  "contact_uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "contact_type",
  { data_type => "text", is_nullable => 0 },
  "relation",
  { data_type => "integer", is_nullable => 0 },
  "relation_uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "relation_type",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-12 19:26:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:yIDaOHdDIbxaLgp8erS1Iw

__PACKAGE__->load_components(
     "+Zaaksysteem::DB::Component::ContactRelation",
    __PACKAGE__->load_components()
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
