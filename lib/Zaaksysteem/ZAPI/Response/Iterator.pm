package Zaaksysteem::ZAPI::Response::Iterator;
use Moose::Role;

use Data::Page;

=head1 NAME

Zaaksysteem::ZAPI::Response::Iterator - ZAPI response role for "Iterator" objects

=head1 SYNOPSIS

    sub my_controller {
        # Assume "SomeIteratorClass" has 'next' and 'reset' methods

        $c->stash->{zapi} = SomeIteratorClass->new(@content);
    }

=head1 METHODS

=head2 _is_iterator

Helper function to determine whether the specified object is of a Zaaksysteem
iterator type.

=cut

sub _is_iterator {
    my $self = shift;
    my ($iter) = @_;

    return unless eval { $iter->isa('Zaaksysteem::Object::Iterator'); };

    return 1;
}

=head2 from_iterator

Fill the internals of the L<ZAPI::Response>, based on the iterator that's
passed in.

=cut

sub from_iterator {
    my $self        = shift;
    my $iterator   = shift;

    die('Not a valid Iterator ' . ref($iterator))
        unless $self->_is_iterator($iterator);

    $self->_input_type('iterator');

    my %pager_options = (
        current_page => $self->page_current
    );

    $pager_options{page_size} = $self->no_pager ? $iterator->count : $self->page_size;

    $iterator->set_pager_attributes(%pager_options);

    $self->_generate_paging_attributes(
        $iterator->pager
    );

    $self->result($iterator);

    $self->_validate_response;

    return $self->response;
}

around from_unknown => sub {
    my $orig        = shift;
    my $self        = shift;
    my ($data)      = @_;

    if ( $self->_is_iterator($data) ) {
        $self->from_iterator(@_);
    }

    $self->$orig( @_ );
};

around '_validate_response' => sub {
    my $method      = shift;
    my $self        = shift;

    if($self->_input_type eq 'iterator') {
        die('Invalid format for result attribute')
            unless $self->_is_iterator($self->result);
    }

    return $self->$method(@_);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
