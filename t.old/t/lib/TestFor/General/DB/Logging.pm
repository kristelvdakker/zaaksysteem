package TestFor::General::DB::Logging;
use base qw(ZSTest);
use TestSetup;

sub zs_logging_create : Tests {
    $zs->txn_ok(
        sub {

            my $log = $zs->create_logging_ok();
            is_deeply($log->data, { some => 'data' }, "Found correct data");

            my $json = $log->TO_JSON;

            # Need stopped clock
            isnt(delete $json->{timestamp}, undef, "We have a timestamp");
            delete $json->{id}; # not important what the ID is

            is_deeply(
                $json,
                {
                    case_id        => undef,
                    created_by     => '',
                    description    => 'Event "foo/bar": {"some":"data"}',
                    event_category => 'foo',
                    event_type     => 'foo/bar',
                    some           => 'data',
                },
                "TO_JSON is correct"
            );

            my $rs = $zs->schema->resultset("Logging");

            #my $event_type = 'case/update/result',
            my $event_type = 'foo/bar',
            my $data = {
                data => {
                    some       => 'data',
                },
            };
            $log = $rs->trigger($event_type, $data);
            is_deeply($log->data, { some => 'data' }, "Found correct data");

            $json = $log->TO_JSON;

            # Need stopped clock
            isnt(delete $json->{timestamp}, undef, "We have a timestamp");
            delete $json->{id}; # not important what the ID is

            is_deeply(
                $json,
                {
                    case_id        => undef,
                    created_by     => '',
                    description    => 'Event "foo/bar": {"some":"data"}',
                    event_category => 'foo',
                    event_type     => 'foo/bar',
                    some           => 'data',
                },
                "TO_JSON is correct"
            );

        },
        "Logging trigger tests"
    );
}

sub zs_logging_find_recent : Tests {
    $zs->txn_ok(
        sub {

            my $rs = $zs->schema->resultset("Logging");

            my $event_type = 'case/update/attributes';
            my $data = { attribute_id => '42', attribute_value => 'foobar', case_id => '42' };

            $rs->trigger('case/update/attributes', { data => $data });

            my $found = $rs->find_recent({
                event_type => $event_type,
                data       => $data,
            });

            ok($found, "We found a result");
            is_deeply($found->data, $data, "Got the most recent update");

        },
    );
}


1;

__END__

=head1 NAME

TestFor::General::DB::Logging - A logging tester

=head1 DESCRIPTION

Testing zaaktypes

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
