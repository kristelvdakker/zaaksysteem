package TestFor::General::Betrokkene;
use base qw(ZSTest);

use Moose;
use TestSetup;
use Log::Log4perl;

use Zaaksysteem::Betrokkene;

=head1 NAME

TestFor::General::Betrokkene - Old style betrokkene handling, minor tests

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/General/Betrokkene.pm

=head1 DESCRIPTION

These tests cover a minor portion of betrokkene handling. When there is a change in betrokkene handling,
we add tests here to cover at least a portion of this old grumpy code.

=head1 USAGE

=head2 NATUURLIJK PERSOON

=head3 betrokkene_np_implementation_addresses

Adding an addres to a betrokkene

=cut

sub betrokkene_np_implementation_addresses : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $betrokkene = $self->_create_natuurlijk_persoon_as_object();

        ok($betrokkene->verblijfsadres, 'Got a valid verblijfsadres');
        is($betrokkene->functie_adres, 'W', 'Got valid functie adres');
    }, 'NP: Checked for valid VERBLIJFS address');

    $zs->zs_transaction_ok(sub {
        my $betrokkene = $self->_create_natuurlijk_persoon_as_object(
            {
                'correspondentie_straatnaam' => 'Corstraat',
                'correspondentie_huisnummer' => '321',
                'correspondentie_woonplaats' => 'Adam',
                'correspondentie_postcode'   => '3211AA',
                'correspondentie_huisnummertoevoeging'   => 'test',
                'correspondentie_huisletter'   => 'a',
            }
        );

        ok($betrokkene->correspondentieadres, 'Got a valid correspondentieadres');
        is($betrokkene->functie_adres, 'B', 'Got valid functie adres');
    }, 'NP: Checked for valid CORRESPONDENTIE address');

    $zs->zs_transaction_ok(sub {
        my $betrokkene = $self->_create_natuurlijk_persoon_as_object(
            {
                'correspondentie_straatnaam' => 'Corstraat',
                'correspondentie_huisnummer' => '321',
                'correspondentie_woonplaats' => 'Adam',
                'correspondentie_postcode'   => '3211AA',
                'correspondentie_huisnummertoevoeging'   => 'test',
                'correspondentie_huisletter'   => 'a',
            }
        );

        my $addresses  = $betrokkene->gm_np->cached_addresses;
        ok(@{ $addresses }, 'Got at least one valid address in cached addresses');

        for my $address (@$addresses) {
            is($address->natuurlijk_persoon_id->id, $betrokkene->gm_np->id, 'Got valid natuurlijk_persoon_id');
        }

        ### Set this betrokkene
        my $bobject     = $self->_get_betrokkene_object;
        my ($cached_id) = $bobject->set($betrokkene->betrokkene_identifier) =~ /-(\d+)$/;

        ok($cached_id, 'Correctly setted this new betrokkene in cache');

        ### And get this betrokkene
        my $cached_betrokkene = $self->_get_betrokkene_object()->get({ type => 'natuurlijk_persoon', intern => 1 }, $cached_id);

        my $cached_addresses  = $cached_betrokkene->gm_np->cached_addresses;
        ok(@{ $cached_addresses }, 'Got at least one valid address in cached addresses');

        for my $address (@$cached_addresses) {
            is($address->natuurlijk_persoon_id->id, $cached_betrokkene->gm_np->id, 'Got valid natuurlijk_persoon_id on cached betrokkene');
        }
    }, 'NP: Checked for valid links between addresses');

    $zs->zs_transaction_ok(sub {
        my $betrokkene = $self->_create_natuurlijk_persoon_as_object(
            {
                'correspondentie_straatnaam' => 'Corstraat',
                'correspondentie_huisnummer' => '321',
                'correspondentie_woonplaats' => 'amstercor',
                'correspondentie_postcode'   => '3211AA',
                'correspondentie_huisnummertoevoeging'   => 'test',
                'correspondentie_huisletter'   => 'a',
            }
        );

        for my $attr (qw/straatnaam huisnummer huisletter huisnummertoevoeging woonplaats/) {
            my $specific_attr = 'correspondentie_' . $attr;

            ok($betrokkene->$attr, 'Found attribute: ' . $attr . ': ' . $betrokkene->$attr);
            is($betrokkene->$specific_attr, $betrokkene->$attr, 'Specific attribute same as normal attribute: ' . $specific_attr);
        }

        ok($betrokkene->correspondentieadres, 'Betrokkene has a correspondentieadres');

        is($betrokkene->adres_id->id, $betrokkene->correspondentieadres->id, 'adres_is is set to correct address');

        $betrokkene->add_address(
            {
                functie_adres   => 'W',
                woonplaats      => 'amstervbl',
            }
        );

        ok($betrokkene->verblijfsadres, 'Betrokkene now has also a verblijfsadres');

        is($betrokkene->adres_id->id, $betrokkene->correspondentieadres->id, 'Adres_id is set to correspondentieadres');

        $betrokkene = $self->_get_betrokkene_object()->get({}, 'betrokkene-natuurlijk_persoon-' . $betrokkene->gm_np->id);

        ### Other woonplaats
        is($betrokkene->woonplaats, 'amstercor', 'Got correct woonplaats');
        is($betrokkene->verblijf_woonplaats, 'amstervbl', 'Got correct verblijf_woonplaats');

    }, 'NP: Add extra addres: Verblijfsadres');

    $zs->zs_transaction_ok(sub {
        my $betrokkene = $self->_create_natuurlijk_persoon_as_object({'functie_adres' => 'W', woonplaats => 'amstervbl'});

        for my $attr (qw/straatnaam huisnummer huisletter huisnummertoevoeging woonplaats/) {
            my $specific_attr = 'verblijf_' . $attr;

            ok($betrokkene->$attr, 'Found attribute: ' . $attr . ': ' . $betrokkene->$attr);
            is($betrokkene->$specific_attr, $betrokkene->$attr, 'Specific attribute same as normal attribute: ' . $specific_attr);
        }

        ok($betrokkene->verblijfsadres, 'Betrokkene has a correspondentieadres');
        ok(!$betrokkene->correspondentieadres, 'Betrokkene has no verblijfsadres, creating');

        is($betrokkene->adres_id->id, $betrokkene->verblijfsadres->id, 'adres_is is set to correct address');

        $betrokkene->add_address(
            {
                functie_adres   => 'B',
                woonplaats      => 'amstercor',
            }
        );

        ok($betrokkene->correspondentieadres, 'Betrokkene now has also a correspondentieadres');

        is($betrokkene->adres_id->id, $betrokkene->correspondentieadres->id, 'Adres_id is set to correspondentieadres');

        $betrokkene = $self->_get_betrokkene_object()->get({}, 'betrokkene-natuurlijk_persoon-' . $betrokkene->gm_np->id);

        ### Other woonplaats
        is($betrokkene->woonplaats, 'amstercor', 'Got correct woonplaats');
        is($betrokkene->verblijf_woonplaats, 'amstervbl', 'Got correct verblijf_woonplaats');
        is($betrokkene->correspondentie_woonplaats, 'amstercor', 'Got correct correspondentie_woonplaats');
    }, 'NP: Add extra addres: Briefadres');

    $zs->zs_transaction_ok(sub {
        my $betrokkene = $self->_create_natuurlijk_persoon_as_object({'functie_adres' => 'W', woonplaats => 'amstervbl', huisnummer => '14', straat => 'blaatstr', postcode => '9876AB'});
        $betrokkene->add_address(
            {
                functie_adres   => 'B',
                woonplaats      => 'amstercor',
                huisnummer      => '12',
                straatnaam      => 'Teststraat',
                postcode        => '1234AB'
            }
        );

        $betrokkene = $self->_get_betrokkene_object()->get({}, 'betrokkene-natuurlijk_persoon-' . $betrokkene->gm_np->id);

        my $case = $zs->create_case_ok(
            aanvragers  => [
                {
                    betrokkene      => $betrokkene->betrokkene_identifier,
                    betrokkene_type => 'natuurlijk_persoon',
                    verificatie     => 'medewerker',
                }
            ]
        );

        for my $attr (qw/woonplaats straatnaam/) {
            my $caseattr = $attr;

            my $attrcor = my $attrcorcase = 'correspondentie_' . $attr;
            my $attrvbl = my $attrvblcase = 'verblijf_' . $attr;
            $attrcorcase =~ s/straatnaam/straat/;
            $attrvblcase =~ s/straatnaam/straat/;
            $caseattr        =~ s/straatnaam/straat/;

            is($case->systeemkenmerk('aanvrager_' . $caseattr), $betrokkene->$attrcor, 'Correspondentie kenmerk matched aanvrager kenmerk: ' . $caseattr);
            is($case->systeemkenmerk('aanvrager_' . $attrcorcase), $betrokkene->$attrcor, 'Correspondentie kenmerk matched aanvrager_correspondentie kenmerk: ' . $attr);
            is($case->systeemkenmerk('aanvrager_' . $attrvblcase), $betrokkene->$attrvbl, 'Correspondentie kenmerk matched aanvrager_verblijf kenmerk: ' . $attr);
        }
    }, 'NP: Check magic strings');

    $zs->zs_transaction_ok(sub {
        my $betrokkene = $self->_create_natuurlijk_persoon_as_object({'functie_adres' => 'W', woonplaats => 'amstervbl'});

        $betrokkene->add_address(
            {
                functie_adres   => 'B',
                woonplaats      => 'amstercor',
            }
        );

        ok($betrokkene->correspondentieadres, 'Betrokkene now has also a correspondentieadres');

        ok($betrokkene->delete_address_by_function('B'), 'Deleted address by code: B');

        my $bobject     = $self->_get_betrokkene_object;
        $betrokkene  = $bobject->get({}, 'betrokkene-natuurlijk_persoon-' . $betrokkene->gm_np->id);

        ok(!$betrokkene->correspondentieadres, 'Betrokkene correspondentieadres succesfully removed');

    }, 'NP: Remove extra address: Briefadres');
}

sub betrokkene_np_implementation_create : Tests {
    my $self = shift;

    my $np_params = {
        'np-burgerservicenummer' => '12345678',
        'np-voorvoegsel' => 'MK',
        'np-geboortedatum' => DateTime->new(day => '1', month => '2', year => '1983'),
        'np-in_gemeente' => '1',
        'np-landcode' => '6030',
        'np-huisnummer' => '12',
        'np-postcode' => '1234ab',
        'np-straatnaam' => 'Dcurtius',
        'np-woonplaats' => 'Amsterdam',
        'np-geslachtsnaam' => 'Fuego',
        'np-voornamen' => 'Don',
        'np-geslachtsaanduiding' => 'M',

        'np-correspondentie_straatnaam' => 'Marnix',
        'np-correspondentie_huisnummer' => '55',
        'np-correspondentie_woonplaats' => 'Amsterdam',
        'np-correspondentie_postcode'   => '4321AB',
    };

    $zs->zs_transaction_ok(sub {
        my $bobject     = $self->_get_betrokkene_object();

        my $id          = $bobject->create(
            'natuurlijk_persoon',
            $np_params,
        );

        my $betrokkene  = $bobject->get({}, 'betrokkene-natuurlijk_persoon-' . $id);

        for my $rawparam (keys %$np_params) {
            my $param = $rawparam;
            $param    =~ s/^np\-//;
            ok($betrokkene->$param, 'Got filled betrokkene accessor for: ' . $param);
        }

        ### Check correspondentie and verblijf
        for my $param (qw/straatnaam huisnummer woonplaats postcode/) {
            my $corparam = 'correspondentie_' . $param;
            my $vblparam = 'verblijf_' . $param;

            ok($betrokkene->$corparam, 'Found correspondentie: ' . $param);
            ok($betrokkene->$vblparam, 'Found verblijf: ' . $param);
        }

        ### Double check
        is($betrokkene->correspondentie_straatnaam, 'Marnix', 'Got correct straatnaam for correspondentieadres');
        is($betrokkene->straatnaam, 'Marnix', 'Got correct straatnaam for native adres');
        is($betrokkene->verblijf_straatnaam, 'Dcurtius', 'Got correct straatnaam for verblijfadres');

        is($betrokkene->functie_adres, 'B', 'Got a briefadres');
    }, 'NP: Create a new NP');

    
    $zs->zs_transaction_ok(sub {
        my $bobject     = $self->_get_betrokkene_object();

        my $changed_params = { %$np_params };
        delete($changed_params->{$_}) for grep(/^np-correspondentie_/, keys %$changed_params);

        my $id          = $bobject->create(
            'natuurlijk_persoon',
            $changed_params,
        );

        my $betrokkene  = $bobject->get({}, 'betrokkene-natuurlijk_persoon-' . $id);

        ok($betrokkene->straatnaam, 'Got a straatnaam for NP');
        ok(!$betrokkene->correspondentie_straatnaam, 'No correspondentie_straatnaam for NP');
        ok($betrokkene->verblijf_straatnaam, 'Got verblijf_straatnaam set for NP');

        is($betrokkene->functie_adres, 'W', 'Got a verblijfsadres');

    }, 'NP: Create a new NP without correspondentie');

    $zs->zs_transaction_ok(sub {
        my $bobject     = $self->_get_betrokkene_object();

        my $changed_params = { %$np_params };
        delete($changed_params->{$_}) for qw/np-woonplaats np-postcode np-straatnaam np-huisnummer/;

        my $id          = $bobject->create(
            'natuurlijk_persoon',
            $changed_params,
        );

        my $betrokkene  = $bobject->get({}, 'betrokkene-natuurlijk_persoon-' . $id);

        ok($betrokkene->straatnaam, 'Got a straatnaam for NP');
        ok($betrokkene->correspondentie_straatnaam, 'No correspondentie_straatnaam for NP');
        ok(!$betrokkene->verblijf_straatnaam, 'Got verblijf_straatnaam set for NP');

        is($betrokkene->functie_adres, 'B', 'Got a briefadres');

    }, 'NP: Create a new NP without verblijf');
}

sub _create_natuurlijk_persoon_as_object {
    my $self    = shift;
    my $args    = shift;

    my $nprow   = $zs->create_natuurlijk_persoon_ok(geslachtsnaam => 'Test', voorletters => 'T', %$args);

    my $bobject = $self->_get_betrokkene_object;

    my $np      = $bobject->get({}, 'betrokkene-natuurlijk_persoon-' . $nprow->id);

    is($np->geslachtsnaam, 'Test', 'Created betrokkene with lastname: Test');

    return $np;
}

sub _get_betrokkene_object {
    my $self    = shift;

    return $self->{_cached_bobject} if $self->{_cached_bobject};

    return $self->{_cached_bobject} = Zaaksysteem::Betrokkene->new(
        'prod'      => 1,
        'dbic'      => $schema,
        'stash'     => {},
        'log'       => Log::Log4perl->get_logger(ref $schema),
    );
}



=head2 BEDRIJF

=head2 MEDEWERKER

Usage tests, use these if you would like to know how to interact with the API.

=head1 IMPLEMENTATION TESTS

=cut

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
