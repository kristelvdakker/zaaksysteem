#! perl

use TestSetup;

# $ENV{CATALYST_DEBUG} = 1;
use Zaaksysteem::TestMechanize 'Zaaksysteem';

initialize_test_globals_ok;

### Announce schema to catalyst
$ENV{ZAAKSYSTEEM_SCHEMA} = $schema;

use Test::Class::Load 't/lib/TestFor/Catalyst';

Test::Class->runtests();
