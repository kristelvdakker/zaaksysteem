#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

use Zaaksysteem::Exception;

initialize_test_globals_ok;

eval {
    throw('error/test','This is a test message');
};

ok(UNIVERSAL::isa($@, 'Zaaksysteem::Exception::Base'), 'Got proper exception');

my $json = $@->TO_JSON;

ok(exists $json->{result}->[0]->{ $_ }, 'Found proper JSON attr: ' . $_) for qw/
    messages
    type
    stacktrace
    data
/;

# ZS-2606
{
    lives_ok {
        try {
            throw('test/error', 'error');
        } grab 'test/error', sub {
        };
    } 'exception caught as expected';

    dies_ok {
        try {
            throw('other/error', 'error');
        } grab 'test/error', sub {
        }
    } 'exception fell through as expected';
}

zs_done_testing();
