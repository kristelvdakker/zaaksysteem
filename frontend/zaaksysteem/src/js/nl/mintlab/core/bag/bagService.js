// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.core.bag').factory('bagService', [
    '$parse',
    function ($parse) {
      var bagService = {},
        streetLabel =
          "streetname + (number && (' ' + number) || '' ) + ', ' + city",
        bagLabel = "street + (number && (' ' + number) || '' ) + ', ' + city";

      bagService.getStreetLabel = function () {
        return streetLabel;
      };

      bagService.getLabel = function () {
        return bagLabel;
      };

      bagService.parseLabel = function (obj) {
        var tpl;
        if (obj.streetname) {
          tpl = streetLabel;
        } else {
          tpl = bagLabel;
        }
        return $parse(tpl)(obj);
      };

      return bagService;
    },
  ]);
})();
