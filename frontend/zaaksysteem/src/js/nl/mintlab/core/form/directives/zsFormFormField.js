// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.form').directive('zsFormFormField', [
    'translationService',
    function (translationService) {
      return {
        scope: true,
        priority: -1,
        require: ['zsFormTemplateParser', 'ngModel'],
        link: function (scope, element, attrs, controllers) {
          var zsForm = controllers[0],
            ngModel = controllers[1],
            fields = angular.copy(scope.$eval(attrs.fields)),
            name = attrs.name,
            config;

          function cancelEvent(event) {
            event.stopPropagation();
          }

          function setModelValue() {
            var formValues = zsForm.getFormValues();

            if (!angular.equals(formValues, ngModel.$modelValue)) {
              ngModel.$setViewValue(angular.copy(formValues));
            }
          }

          function setViewValue() {
            var modelValues = ngModel.$modelValue,
              formValues = zsForm.getFormValues(),
              form = zsForm.getForm();

            if (!angular.equals(modelValues, formValues)) {
              for (var key in modelValues) {
                zsForm.setValue(key, modelValues[key]);
              }
            }
          }

          function setValidity() {
            var valid = zsForm.isFormValid();

            ngModel.$setValidity('zs-valid', valid);
            ngModel.$setValidity('ng-required', valid);
          }

          config = {
            name: name,
            options: {
              autosave: true,
            },
            actions: [
              {
                id: 'submit',
                label: translationService.get('Opslaan'),
                type: 'submit',
              },
            ],
            fields: fields,
          };

          ngModel.$formatters.push(function (value) {
            var values = angular.copy(value),
              form = zsForm.getForm();

            _.each(values, function (value, key) {
              var field;

              if (form) {
                form.setValue(key, value);
              } else {
                field = _.find(config.fields, { name: key });
                if (field) {
                  field.value = value;
                }
              }
            });

            setValidity();
            return value;
          });

          ngModel.$parsers.push(function (value) {
            setValidity();
            return value;
          });

          scope.getFormConfig = function () {
            return config;
          };

          scope.$on('form.change.committed', cancelEvent);
          scope.$on('form.prepare', cancelEvent);
          scope.$on('form.change', cancelEvent);
          scope.$on('form.ready', function (event) {
            cancelEvent(event);
            setViewValue();
            setValidity();
          });

          scope.$on('form.submit.attempt', function (event) {
            cancelEvent(event);
            setModelValue();
          });
        },
      };
    },
  ]);
})();
