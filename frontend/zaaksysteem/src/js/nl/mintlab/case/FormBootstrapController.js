// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.case')
    .controller('nl.mintlab.case.FormBootstrapController', [
      '$scope',
      'contextualActionService',
      function ($scope, contextualActionService) {
        $scope.openFormBootstrapPopup = function () {
          contextualActionService.openAction(
            contextualActionService.findActionByName('zaak'),
            {
              casetypeId: $scope.casetype_id,
              requestor: {
                type: $scope.requestor_type,
                label: $scope.requestor_name,
                data: {
                  id: $scope.identifier,
                  uuid: $scope.requestor_id,
                },
              },
            }
          );
        };
      },
    ]);
})();
