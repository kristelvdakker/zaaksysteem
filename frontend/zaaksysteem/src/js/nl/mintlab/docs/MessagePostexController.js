/*global angular,_*/
angular
  .module('Zaaksysteem.docs')
  .controller('nl.mintlab.docs.MessagePostexController', [
    '$scope', '$rootScope', '$q', '$http', 'smartHttp', 'translationService', 'systemMessageService', 'snackbarService', 'messagePostexService',
    function ($scope, $rootScope, $q, $http, smartHttp, translationService, systemMessageService, snackbarService, messagePostexService) {

      var loading = false;

      $scope.init = function () {

      }

      $scope.maySubmit = function () {

        if ($scope.loading) {
          return false;
        }

        return !Boolean( _.find($scope.attachments, function (file, i) {
          return file.extension_dotless != 'pdf';
        }) );

      }

      $scope.submit = function () {

        $scope.loading = true;

        snackbarService
          .wait('Bezig met versturen...', {
            promise: messagePostexService.submit(
              $scope.postexInterfaceUUID,
              $scope.requestor.uuid,
              {
                recipient_type: 'aanvrager',
                log_error: 0,
                case_id: $scope.case.instance.number,
                subject: $scope.subject,
                body: $scope.content,
                file_attachments: _.map($scope.attachments, function (file, i) {
                  return file.id;
                })
              }),
            then: function (response) {
              return 'Het bericht is afgeleverd bij Postex.';
            },
            catch: function (error) {
              if (_.get(error, 'data.result.instance.message')) {
                return _.get(error, 'data.result.instance.message');
              }
              else if (_.get(error, 'data.result.preview')) {
                return _.get(error, 'data.result.preview');
              }
              else if (typeof error === "string") {
                return error;
              }
              else {
                return 'Er is iets fout gegaan bij het versturen van het bericht. Neem contact op met de beheerder';
              }
            },
            finally: function(){
              $scope.loading = false;
            }
          })
          .then(function () {
            $scope.closePopup();
          })
          .finally(function () {
            $scope.loading = false;
          });
      }
    }
  ]);
