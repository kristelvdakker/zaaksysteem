// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem.filters').filter('capitalize', function () {
    return function (from) {
      return from ? from[0].toUpperCase() + from.substr(1) : '';
    };
  });
})();
